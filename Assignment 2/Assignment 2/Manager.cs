﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 02/01/2015
// Description: Extends the user class for user's who's role is manager.
// Version    : 1.0
//-----------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public class Manager : User
    {
        /// <summary>
        /// Adds new user details to the database.
        /// </summary>
        /// <param name="username">A string representing the new user's username.</param>
        /// <param name="password">A string representing the new user's password.</param>
        /// <param name="role">An integer representing the role of the user.</param>
        public bool CreateUser(string username, string password, int role)
        {
            bool userCreated;

            userName = username;
            userPassword = password;
            userRole = role;

            if (CheckNewUserDetails())
            {
                data.InsertUser(userName, userPassword, userRole);
                userCreated = true;
            }
            else
            {
                userCreated = false;
            }

            return userCreated;
        }

        /// <summary>
        /// Parses a string containing an enum user role value and converts it to an integer.
        /// </summary>
        /// <param name="userRoleString">A string representing the enum value of the selected user role.</param>
        /// <returns>An integer representing the selected user role.</returns>
        public int SetUserRole(string userRoleString)
        {
            UserRole role;
            Enum.TryParse<UserRole>(userRoleString, out role);

            int userRoleInt = (int)role;

            return userRoleInt;
        }

        /// <summary>
        /// Checks that the user details input by the manager are valid.
        /// </summary>
        /// <returns>A boolean to indicate whether or not the details valid.</returns>
        public bool CheckNewUserDetails()
        {
            bool goodInputs = true;

            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(userPassword))
            {
                goodInputs = false;
            }

            return goodInputs;
        }
    }
}
