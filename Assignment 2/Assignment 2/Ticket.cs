﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 09/12/2014
// Description: Create, view, and edit Tickets.
// Version    : 1.0
//-----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Assignment_2
{
    /// <summary>
    /// Enum for levels of ticket priority.
    /// </summary>
    public enum TicketPriority
    {
        Critical,
        High,
        Medium,
        Low
    };

    /// <summary>
    /// Enum for status of ticket.
    /// </summary>
    public enum TicketStatus
    {
        Open,
        Waiting,
        Closed
    };

    /// <summary>
    /// Enum for type of ticket.
    /// </summary>
    public enum TicketType
    {
        Requirement,
        Issue
    };

    public class Ticket
    {
        private string ticketTitle;
        private string ticketDescription;
        private int ticketPriority;
        private int ticketStatus;
        private int ticketProjectID;
        private int ticketType;
        private int ticketUserID;

        private DataAccess data = new DataAccess();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Ticket()
        {

        }

        /// <summary>
        /// Parameterised constructor for creating a new ticket to add to the database.
        /// </summary>
        /// <param name="title">A string containing the title of the ticket</param>
        /// <param name="description">A string containing a description of the project.</param>
        /// <param name="priority">An integer representing the priority of the ticket.</param>
        /// <param name="status">An integer representing the status of the ticket.</param>
        /// <param name="projectID">The ID of the project the ticket is related to.</param>
        /// <param name="type">An integer representing the type of the ticket.</param>
        /// <param name="userID">The ID of the user associated with the ticket.</param>
        public Ticket(string title, string description, int priority, int status, int projectID, int type, int userID)
        {
            ticketTitle = title;
            ticketDescription = description;
            ticketPriority = priority;
            ticketStatus = status;
            ticketProjectID = projectID;
            ticketType = type;
            ticketUserID = userID;
        }

        /// <summary>
        /// Creates a new ticket.
        /// </summary>
        public void CreateTicket()
        {
            data.InsertTicket(ticketTitle, ticketDescription, ticketPriority, ticketStatus, ticketProjectID, ticketType, ticketUserID);
        }

        /// <summary>
        /// Updates the ticket's details.
        /// </summary>
        /// <param name="projectID"></param>
        public void EditTicket(int ticketID)
        {
            data.UpdateTicket(ticketTitle, ticketDescription, ticketPriority, ticketStatus, ticketProjectID, ticketType, ticketUserID, ticketID);
        }

        /// <summary>
        /// Retrieves all tickets associated with a selected project.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <returns>A DataTable containing all tickets associated with the selected project.</returns>
        public DataTable GetProjectTickets(int projectID)
        {
            DataTable projectTickets = new DataTable();

            projectTickets = data.SelectProjectTickets(projectID);

            return projectTickets;
        }

        /// <summary>
        /// Retrieves all tickets of a specific type and status associated with a selected project.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <param name="type">An integer representing the type of tickets to retrieve.</param>
        /// <param name="status">An integer representing the status of tickets to retrieve.</param>
        /// <returns>A DataTable containing all tickets of a specific type and status associated with the selected project.</returns>
        public DataTable GetProjectTicketsByTypeAndStatus(int projectID, int type, int status)
        {
            DataTable projectTickets = new DataTable();

            projectTickets = data.SelectProjectTicketsByTypeAndStatus(projectID, type, status);

            return projectTickets;
        }

        /// <summary>
        /// Retrieves all tickets of a specific priority and status associated with a selected project.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <param name="priority">An integer representing the priority of tickets to retrieve.</param>
        /// <param name="status">An integer representing the status of tickets to retrieve.</param>
        /// <returns>A DataTable containing all tickets of a specific priority and status associated with the selected project.</returns>
        public DataTable GetProjectTicketsByPriorityAndStatus(int projectID, int priority, int status)
        {
            DataTable projectTickets = new DataTable();

            projectTickets = data.SelectProjectTicketsByPriorityAndStatus(projectID, priority, status);

            return projectTickets;
        }

        /// <summary>
        /// Retrieves all tickets of a specific status associated with a selected user.
        /// </summary>
        /// <param name="userID">The ID of the selected user.</param>
        /// <param name="status">An integer representing the status of tickets to retrieve.</param>
        /// <returns>A DataTable containing all tickets of a specific status associated with the selected user.</returns>
        public DataTable GetTicketsByUserAndStatus(int userID, int status)
        {
            DataTable projectTickets = new DataTable();

            projectTickets = data.SelectTicketsByUserAndStatus(userID, status);

            return projectTickets;
        }

        /// <summary>
        /// Parses a string containing an enum ticket priority value and converts it to an integer.
        /// </summary>
        /// <param name="ticketPriorityString">A string representing the enum value of the selected ticket priority.</param>
        /// <returns>An integer representing the selected ticket priority.</returns>
        public int TicketPriorityInt(string ticketPriorityString)
        {
            TicketPriority priority;
            Enum.TryParse<TicketPriority>(ticketPriorityString, out priority);

            int ticketPriorityInt = (int)priority;

            return ticketPriorityInt;
        }

        /// <summary>
        /// Parses a string containing an enum ticket status value and converts it to an integer.
        /// </summary>
        /// <param name="ticketStatusString">A string representing the enum value of the selected ticket status.</param>
        /// <returns>An integer representing the selected ticket status.</returns>
        public int TicketStatusInt(string ticketStatusString)
        {
            TicketStatus status;
            Enum.TryParse<TicketStatus>(ticketStatusString, out status);

            int ticketStatusInt = (int)status;
            return ticketStatusInt;
        }

        /// <summary>
        /// Parses a string containing an enum ticket type value and converts it to an integer.
        /// </summary>
        /// <param name="ticketTypeString">A string representing the enum value of the selected ticket type.</param>
        /// <returns>An integer representing the selected ticket type.</returns>
        public int TicketTypeInt(string ticketTypeString)
        {
            TicketType type;
            Enum.TryParse<TicketType>(ticketTypeString, out type);

            int ticketTypeInt = (int)type;
            return ticketTypeInt;
        }

        /// <summary>
        /// Retrieves the ID of the project associated with a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>An integer representing the project the selected ticket is associated with.</returns>
        public int TicketProjectIDInt(int ticketID)
        {
            int projectID;

            // Get the ID of the project associated with the selected ticket.
            projectID = (int)data.SelectTicket(ticketID).Rows[0][4];

            return projectID;
        }

        /// <summary>
        /// Retrieves a selected ticket's title.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the selected ticket's title.</returns>
        public string TicketTitleString(int ticketID)
        {
            string ticketTitle;

            ticketTitle = data.SelectTicket(ticketID).Rows[0][0].ToString();

            return ticketTitle;
        }

        /// <summary>
        /// Retrieves a selected ticket's description.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the selected ticket's description.</returns>
        public string TicketDescriptionString(int ticketID)
        {
            string ticketDescription;

            ticketDescription = data.SelectTicket(ticketID).Rows[0][1].ToString();

            return ticketDescription;
        }

        /// <summary>
        /// Retrieves the priority of a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the priority of the selected ticket.</returns>
        public string TicketPriorityString(int ticketID)
        {
            int ticketPriorityInt;
            string ticketPriorityString;

            // Get the integer representation of the selected ticket's priority.
            ticketPriorityInt = (int)data.SelectTicket(ticketID).Rows[0][2];

            // Use the ticket priority integer to get the ticket priority enum and convert it into a string.
            TicketPriority ticketPriorityEnum = (TicketPriority)ticketPriorityInt;
            ticketPriorityString = ticketPriorityEnum.ToString();

            return ticketPriorityString;
        }

        /// <summary>
        /// Retrieves the status of a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the status of the selected ticket.</returns>
        public string TicketStatusString(int ticketID)
        {
            int ticketStatusInt;
            string ticketStatusString;

            // Get the integer representation of the selected ticket's status.
            ticketStatusInt = (int)data.SelectTicket(ticketID).Rows[0][3];

            // Use the ticket status integer to get the ticket status enum and convert it into a string.
            TicketStatus ticketStatusEnum = (TicketStatus)ticketStatusInt;
            ticketStatusString = ticketStatusEnum.ToString();

            return ticketStatusString;
        }

        /// <summary>
        /// Retrieves the title of the project associated with a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the title of the project associated with the selected ticket.</returns>
        public string TicketProjectTitleString(int ticketID)
        {
            int projectID;
            string ticketProjectTitle;

            // Get the ID of the project associated with the selected ticket.
            projectID = (int)data.SelectTicket(ticketID).Rows[0][4];

            // Use the project ID to get the project's title.
            ticketProjectTitle = data.SelectProject(projectID).Rows[0][0].ToString();

            return ticketProjectTitle;
        }

        /// <summary>
        /// Retrieves the type of a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the type of the selected ticket.</returns>
        public string TicketTypeString(int ticketID)
        {
            int ticketTypeInt;
            string ticketTypeString;

            // Get the integer representation of the selected ticket's type.
            ticketTypeInt = (int)data.SelectTicket(ticketID).Rows[0][5];

            // Use the ticket type integer to get the ticket type enum and convert it into a string.
            TicketType ticketTypeEnum = (TicketType)ticketTypeInt;
            ticketTypeString = ticketTypeEnum.ToString();

            return ticketTypeString;
        }

        /// <summary>
        /// Retrieves the username of the user associated with a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A string containing the username of the user associated with the selected ticket.</returns>
        public string TicketOwnerString(int ticketID)
        {
            int userID;
            string ticketOwner;

            // Get the ID of the user associated with the selected ticket.
            userID = (int)data.SelectTicket(ticketID).Rows[0][6];

            // Use the user ID to get the user's username.
            ticketOwner = data.SelectUser(userID).Rows[0][0].ToString();

            return ticketOwner;
        }

        /// <summary>
        /// Checks that the ticket details input by the user are valid.
        /// </summary>
        /// <returns>A boolean to indicate whether or not the details valid.</returns>
        public bool CheckNewTicketDetails()
        {
            bool goodInputs = true;

            if (string.IsNullOrWhiteSpace(ticketTitle) || string.IsNullOrWhiteSpace(ticketDescription))
            {
                goodInputs = false;
            }

            return goodInputs;
        }
    }
}
