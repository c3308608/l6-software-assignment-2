﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 09/12/2014
// Description: Provides methods for accessing the database.
// Version    : 1.0
//-----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlServerCe;

namespace Assignment_2
{
    public class DataAccess
    {

        public DataAccess()
        {
        }

        /// <summary>
        /// Creates a connection string containing the path to connect to the database.
        /// </summary>
        /// <returns>A string to use for connecting to the database.</returns>
        public string ConnectionString()
        {
            string connectionString = @"Data Source=C:\Uni\Advanced Software A\ASEA Assignment 2\Assignment2Database.sdf";

            return connectionString;
        }

        #region Project Data Access

        /// <summary>
        /// Selects all projects from the database and puts them into a DataTable.
        /// </summary>
        /// <returns>A DataTable containing all projects from the database.</returns>
        public DataTable SelectAllProjects()
        {
            // Create a DataTable to hold all projects.
            DataTable projects = new DataTable();

            // SQL query to select all project records from the database.
            string selectProjects = "SELECT ProjectID, ProjectTitle, ProjectDescription, ProjectDeadline " +
                                    "FROM Project " +
                                    "ORDER BY ProjectDeadline";

            try
            {
                using(SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectProjectsCommand = new SqlCeCommand(selectProjects, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectProjectsCommand);

                        // Fill the DataTable with all projects from the database.
                        dataAdapter.Fill(projects);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return projects;
        }

        /// <summary>
        /// Selects a specific row from the database and puts it into a DataTable.
        /// </summary>
        /// <param name="projectID">An integer that represents the selected project.</param>
        /// <returns>A DataTable containing the selected project.</returns>
        public DataTable SelectProject(int projectID)
        {
            // Create a DataTable to hold the selected project.
            DataTable project = new DataTable();

            // SQL query to select a specific project from the database.
            string selectProject = "SELECT ProjectTitle, ProjectDescription, ProjectDeadline " +
                                   "FROM Project " +
                                   "WHERE ProjectID = @projectID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectProjectCommand = new SqlCeCommand(selectProject, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectProjectCommand);

                        selectProjectCommand.Parameters.AddWithValue("@projectID", projectID);

                        // Fill the DataTable with the selected project from the database.
                        dataAdapter.Fill(project);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return project;
        }

        /// <summary>
        /// Inserts a new project record into the database.
        /// </summary>
        /// <param name="title">A string containing the project's title.</param>
        /// <param name="description">A string containing a description of the project.</param>
        /// <param name="deadline">A DateTime representing the deadline of the project.</param>
        public void InsertProject(string title, string description, DateTime deadline)
        {

            // SQL query to insert a new project record into the database.
            string insertProject = "INSERT INTO Project(ProjectTitle, ProjectDescription, projectDeadline) " + 
                                   "VALUES (@title, @description, @deadline)";
             
            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    SqlCeCommand insertProjectCommand = new SqlCeCommand(insertProject, sqlConnection);

                    sqlConnection.Open();

                    insertProjectCommand.Parameters.AddWithValue("@title", title);
                    insertProjectCommand.Parameters.AddWithValue("@description", description);
                    insertProjectCommand.Parameters.AddWithValue("@deadline", deadline);
                    insertProjectCommand.ExecuteNonQuery();
                    
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }
        }

        /// Updates a project record in the database.
        /// </summary>
        /// <param name="title">A string containing the project's title.</param>
        /// <param name="description">A string containing a description of the project.</param>
        /// <param name="deadline">A DateTime representing the deadline of the project.</param>
        public void UpdateProject(string title, string description, DateTime deadline, int projectID)
        {

            // SQL query to update a project record in the database.
            string updateProject = "UPDATE Project " +
                                   "SET ProjectTitle = @title, ProjectDescription = @description, projectDeadline = @deadline " +
                                   "WHERE ProjectID = @projectID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    SqlCeCommand updateProjectCommand = new SqlCeCommand(updateProject, sqlConnection);

                    sqlConnection.Open();

                    updateProjectCommand.Parameters.AddWithValue("@title", title);
                    updateProjectCommand.Parameters.AddWithValue("@description", description);
                    updateProjectCommand.Parameters.AddWithValue("@deadline", deadline);
                    updateProjectCommand.Parameters.AddWithValue("@projectID", projectID);
                    updateProjectCommand.ExecuteNonQuery();

                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }
        }

        #endregion

        #region Ticket Data Access

        /// <summary>
        /// Inserts a new ticket record into the database.
        /// </summary>
        /// <param name="title">A string containing the ticket's title.</param>
        /// <param name="description">A string containing the ticket's description.</param>
        /// <param name="priority">An integer representing the tickets priority level.</param>
        /// <param name="status">An integer representing the ticket's status.</param>
        /// <param name="projectID">An intereger representing the project the ticket is associated with.</param>
        /// <param name="type">An integer representing the type of the ticket.</param>
        /// <param name="userID">An integer representing the user the ticket is associated with.</param>
        public void InsertTicket(string title, string description, int priority, int status, int projectID, int type, int userID)
        {

            // SQL query to insert a new project record into the database.
            string insertTicket = "INSERT INTO Ticket(TicketTitle, TicketDescription, TicketPriority, TicketStatus, TicketProjectID, TicketType, TicketUserID) " +
                                  "VALUES (@title, @description, @priority, @status, @projectID, @type, @userID)";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    SqlCeCommand insertTicketCommand = new SqlCeCommand(insertTicket, sqlConnection);

                    sqlConnection.Open();

                    insertTicketCommand.Parameters.AddWithValue("@title", title);
                    insertTicketCommand.Parameters.AddWithValue("@description", description);
                    insertTicketCommand.Parameters.AddWithValue("@priority", priority);
                    insertTicketCommand.Parameters.AddWithValue("@status", status);
                    insertTicketCommand.Parameters.AddWithValue("@projectID", projectID);
                    insertTicketCommand.Parameters.AddWithValue("@type", type);
                    insertTicketCommand.Parameters.AddWithValue("@userID", userID);
                    insertTicketCommand.ExecuteNonQuery();

                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

        }

        /// <summary>
        /// Selects all tickets from the database associated with a specific project and puts them into a DataTable.
        /// </summary>
        /// <param name="projectID">An integer that represents the selected project.</param>
        /// <returns>A DataTable containing all tickets for a specific project from the database.</returns>
        public DataTable SelectProjectTickets(int projectID)
        {
            // Create a DataTable to hold tickets.
            DataTable projectTickets = new DataTable();

            // SQL query to select all ticket records associated with the selected project from the database.
            string selectProjectTickets = "SELECT TicketID, TicketTitle, TicketDescription, TicketPriority " +
                                          "TicketStatus, TicketProjectID, TicketType, TicketUserID " +
                                          "FROM Ticket " +
                                          "WHERE TicketProjectID = @projectID " +
                                          "ORDER BY TicketTitle";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectProjectTicketsCommand = new SqlCeCommand(selectProjectTickets, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectProjectTicketsCommand);

                        selectProjectTicketsCommand.Parameters.AddWithValue("@projectID", projectID);

                        // Fill the DataTable with all tickets associated with the selected project from the database.
                        dataAdapter.Fill(projectTickets);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return projectTickets;
        }

        /// <summary>
        /// Selects all tickets of a specific type and status associated with a specific project from the database
        /// and puts them into a DataTable.
        /// </summary>
        /// <param name="projectID">An integer that represents the selected project.</param>
        /// <param name="type">An integer representing the type of the tickets to select.</param>
        /// <param name="status">An integer representing the status of the tickets to select.</param>
        /// <returns>A DataTable containing all tickets for a specific project from the database.</returns>
        public DataTable SelectProjectTicketsByTypeAndStatus(int projectID, int type, int status)
        {
            // Create a DataTable to hold tickets.
            DataTable projectTickets = new DataTable();

            // SQL query to select all ticket records of a specific type and status associated with the selected project from the database.
            string selectProjectTickets = "SELECT TicketID, TicketTitle, TicketDescription, TicketPriority " +
                                          "TicketStatus, TicketProjectID, TicketType, TicketUserID " +
                                          "FROM Ticket " +
                                          "WHERE TicketProjectID = @projectID " +
                                          "AND TicketType = @type " +
                                          "AND TicketStatus = @status " +
                                          "ORDER BY TicketTitle";


            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectProjectTicketsCommand = new SqlCeCommand(selectProjectTickets, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectProjectTicketsCommand);

                        selectProjectTicketsCommand.Parameters.AddWithValue("@projectID", projectID);
                        selectProjectTicketsCommand.Parameters.AddWithValue("@type", type);
                        selectProjectTicketsCommand.Parameters.AddWithValue("@status", status);

                        // Fill the DataTable with all tickets of a specfic type and status associated with the selected project from the database.
                        dataAdapter.Fill(projectTickets);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return projectTickets;
        }

        /// <summary>
        /// Selects all tickets of a specific priority and status associated with a specific project from the database
        /// and puts them into a DataTable.
        /// </summary>
        /// <param name="projectID">An integer that represents the selected project.</param>
        /// <param name="type">An integer representing the priority of the tickets to select.</param>
        /// <param name="status">An integer representing the status of the tickets to select.</param>
        /// <returns>A DataTable containing all tickets for a specific project from the database.</returns>
        public DataTable SelectProjectTicketsByPriorityAndStatus(int projectID, int priority, int status)
        {
            // Create a DataTable to hold tickets.
            DataTable projectTickets = new DataTable();

            // SQL query to select all ticket records of a specific priority and status associated with the selected project from the database.
            string selectProjectTickets = "SELECT TicketID, TicketTitle, TicketDescription, TicketPriority " +
                                          "TicketStatus, TicketProjectID, TicketType, TicketUserID " +
                                          "FROM Ticket " +
                                          "WHERE TicketProjectID = @projectID " +
                                          "AND TicketPriority = @priority " +
                                          "AND TicketStatus = @status " +
                                          "ORDER BY TicketTitle";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectProjectTicketsCommand = new SqlCeCommand(selectProjectTickets, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectProjectTicketsCommand);

                        selectProjectTicketsCommand.Parameters.AddWithValue("@projectID", projectID);
                        selectProjectTicketsCommand.Parameters.AddWithValue("@priority", priority);
                        selectProjectTicketsCommand.Parameters.AddWithValue("@status", status);

                        // Fill the DataTable with all tickets of a specfic priority and status associated with the selected project from the database.
                        dataAdapter.Fill(projectTickets);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return projectTickets;
        }

        /// <summary>
        /// Selects all tickets with a specific status associated with a specific user from the database
        /// and puts them into a DataTable.
        /// </summary>
        /// <param name="userID">An integer that represents the selected user.</param>
        /// <param name="status">An integer representing the status of the tickets to select.</param>
        /// <returns>A DataTable containing all tickets for a specific project from the database.</returns>
        public DataTable SelectTicketsByUserAndStatus(int userID, int status)
        {
            // Create a DataTable to hold tickets.
            DataTable userTickets = new DataTable();

            // SQL query to select all ticket records of a specific status associated with the selected user from the database.
            string selectUserTickets = "SELECT TicketID, TicketTitle, TicketDescription, TicketPriority " +
                                       "TicketStatus, TicketProjectID, TicketType, TicketUserID " +
                                       "FROM Ticket " +
                                       "WHERE TicketUserID = @userID " +
                                       "AND TicketStatus = @status " +
                                       "ORDER BY TicketPriority";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectUserTicketsCommand = new SqlCeCommand(selectUserTickets, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectUserTicketsCommand);

                        selectUserTicketsCommand.Parameters.AddWithValue("@userID", userID);
                        selectUserTicketsCommand.Parameters.AddWithValue("@status", status);

                        // Fill the DataTable with all tickets of a specfic status associated with the selected user from the database.
                        dataAdapter.Fill(userTickets);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return userTickets;
        }

        /// <summary>
        /// Selects a specific ticket from the database and puts it into a DataTable.
        /// </summary>
        /// <param name="ticketID">An integer that represents the selected ticket.</param>
        /// <returns>A DataTable containing the selected project.</returns>
        public DataTable SelectTicket(int ticketID)
        {
            // Create a DataTable to hold the selected ticket.
            DataTable ticket = new DataTable();

            // SQL query to select a specific ticket from the database.
            string selectTicket = "SELECT TicketTitle, TicketDescription, TicketPriority, " +
                                  "TicketStatus, TicketProjectID, TicketType, TicketUserID " +
                                  "FROM Ticket " +
                                  "WHERE TicketID = @ticketID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectTicketCommand = new SqlCeCommand(selectTicket, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectTicketCommand);

                        selectTicketCommand.Parameters.AddWithValue("@ticketID", ticketID);

                        // Fill the DataTable with the selected ticket from the database.
                        dataAdapter.Fill(ticket);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return ticket;
        }

        /// <summary>
        /// Updates a ticket record in the database.
        /// </summary>
        /// <param name="title">A string containing the ticket's title.</param>
        /// <param name="description">A string containing the ticket's description.</param>
        /// <param name="priority">An integer representing the tickets priority level.</param>
        /// <param name="status">An integer representing the ticket's status.</param>
        /// <param name="projectID">An intereger representing the project the ticket is associated with.</param>
        /// <param name="type">An integer representing the type of the ticket.</param>
        /// <param name="userID">An integer representing the user the ticket is associated with.</param>
        public void UpdateTicket(string title, string description, int priority, int status, int projectID, int type, int userID, int ticketID)
        {

            // SQL query to update a ticket record in the database.
            string updateTicket = "UPDATE Ticket " +
                                  "SET TicketTitle = @title, TicketDescription = @description, TicketPriority = @priority, " +
                                  "TicketStatus = @status, TicketProjectID = @projectID, TicketType = @type, " +
                                  "TicketUserID = @userID " +
                                  "WHERE TicketID = @ticketID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    SqlCeCommand updateTicketCommand = new SqlCeCommand(updateTicket, sqlConnection);

                    sqlConnection.Open();

                    updateTicketCommand.Parameters.AddWithValue("@title", title);
                    updateTicketCommand.Parameters.AddWithValue("@description", description);
                    updateTicketCommand.Parameters.AddWithValue("@priority", priority);
                    updateTicketCommand.Parameters.AddWithValue("@status", status);
                    updateTicketCommand.Parameters.AddWithValue("@projectID", projectID);
                    updateTicketCommand.Parameters.AddWithValue("@type", type);
                    updateTicketCommand.Parameters.AddWithValue("@userID", userID);
                    updateTicketCommand.Parameters.AddWithValue("@ticketID", ticketID);
                    updateTicketCommand.ExecuteNonQuery();

                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }
        }

        #endregion

        #region User Data Access

        /// <summary>
        /// Inserts a new user record into the database.
        /// </summary>
        /// <param name="username">A string containing the user's username.</param>
        /// <param name="password">A string containing the user's password.</param>
        /// <param name="role">An integer representing the user's role.</param>
        public void InsertUser(string username, string password, int role)
        {

            // SQL query to insert a new user record into the database.
            string insertUser = "INSERT INTO [User](Username, Password, Role) " +
                                "VALUES (@username, @password, @role)";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    SqlCeCommand insertUserCommand = new SqlCeCommand(insertUser, sqlConnection);

                    sqlConnection.Open();

                    insertUserCommand.Parameters.AddWithValue("@username", username);
                    insertUserCommand.Parameters.AddWithValue("@password", password);
                    insertUserCommand.Parameters.AddWithValue("@role", role);
                    insertUserCommand.ExecuteNonQuery();

                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

        }

        /// <summary>
        /// Selects all users from the database and puts them into a DataTable.
        /// </summary>
        /// <returns>A DataTable containing all users from the database.</returns>
        public DataTable SelectAllUsers()
        {
            // Create a DataTable to hold all users.
            DataTable users = new DataTable();

            // SQL query to select all user records from the database.
            string selectUsers = "SELECT UserID, Username, Password, Role " +
                                 "FROM [User]";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectUsersCommand = new SqlCeCommand(selectUsers, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectUsersCommand);

                        // Fill the DataTable with all users from the database.
                        dataAdapter.Fill(users);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return users;
        }

        /// <summary>
        /// Selects a specific user from the database based on the user ID and puts it into a DataTable.
        /// </summary>
        /// <param name="userID">An integer that represents the selected user.</param>
        /// <returns>A DataTable containing the selected user.</returns>
        public DataTable SelectUser(int userID)
        {
            // Create a DataTable to hold the selected user.
            DataTable user = new DataTable();

            // SQL query to select a specific user from the database.
            string selectUser = "SELECT Username, Password, Role " +
                                "FROM [User] " +
                                "WHERE UserID = @userID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectUserCommand = new SqlCeCommand(selectUser, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectUserCommand);

                        selectUserCommand.Parameters.AddWithValue("@userID", userID);

                        // Fill the DataTable with the selected user from the database.
                        dataAdapter.Fill(user);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return user;
        }

        /// <summary>
        /// Selects a specific user from the database based on the username and password and puts it into a DataTable.
        /// </summary>
        /// <param name="username">A string containing a username.</param>
        /// <param name="password">A string containing a password.</param>
        /// <returns>A DataTable containing the selected user.</returns>
        public DataTable SelectUser(string username, string password)
        {
            // Create a DataTable to hold the selected user.
            DataTable user = new DataTable();

            // SQL query to select a specific user from the database.
            string selectUser = "SELECT UserID, Username, Password, Role " +
                                "FROM [User] " +
                                "WHERE Username = @username " + 
                                "AND Password = @password";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectUserCommand = new SqlCeCommand(selectUser, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectUserCommand);

                        selectUserCommand.Parameters.AddWithValue("@username", username);
                        selectUserCommand.Parameters.AddWithValue("@password", password);

                        // Fill the DataTable with the selected user from the database.
                        dataAdapter.Fill(user);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return user;
        }

        #endregion

        #region Comment Data Access

        /// <summary>
        /// Inserts a new comment record into the database.
        /// </summary>
        /// <param name="text">A string containing the comment's text.</param>
        /// <param name="ticketID">An integer representing the ticket the comment is associated with.</param>
        public void InsertComment(string text, int ticketID)
        {

            // SQL query to insert a new comment record into the database.
            string insertComment = "INSERT INTO Comment(CommentText, CommentTicketID) " +
                                   "VALUES (@text, @ticketID)";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    SqlCeCommand insertCommentCommand = new SqlCeCommand(insertComment, sqlConnection);

                    sqlConnection.Open();

                    insertCommentCommand.Parameters.AddWithValue("@text", text);
                    insertCommentCommand.Parameters.AddWithValue("@ticketID", ticketID);
                    insertCommentCommand.ExecuteNonQuery();

                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

        }

        /// <summary>
        /// Selects all comments from the database associated with a specific ticket and puts them into a DataTable.
        /// </summary>
        /// <param name="ticketID">An integer that represents the selected ticket.</param>
        /// <returns>A DataTable containing all comments for a specific ticket from the database.</returns>
        public DataTable SelectTicketComments(int ticketID)
        {
            // Create a DataTable to hold comments.
            DataTable ticketComments = new DataTable();

            // SQL query to select all comment records associated with the selected ticket from the database.
            string selectTicketComments = "SELECT CommentID, CommentText, CommentTicketID " +
                                          "FROM Comment " +
                                          "WHERE CommentTicketID = @ticketID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectTicketCommentsCommand = new SqlCeCommand(selectTicketComments, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectTicketCommentsCommand);

                        selectTicketCommentsCommand.Parameters.AddWithValue("@ticketID", ticketID);

                        // Fill the DataTable with all comments associated with the selected ticket from the database.
                        dataAdapter.Fill(ticketComments);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return ticketComments;
        }

        /// <summary>
        /// Selects a specific comment from the database and puts it into a DataTable.
        /// </summary>
        /// <param name="commentID">An integer that represents the selected comment.</param>
        /// <returns>A DataTable containing the selected comment.</returns>
        public DataTable SelectComment(int commentID)
        {
            // Create a DataTable to hold the selected comment.
            DataTable comment = new DataTable();

            // SQL query to select a specific comment from the database.
            string selectComment = "SELECT CommentText " +
                                   "FROM Comment " +
                                   "WHERE CommentID = @commentID";

            try
            {
                using (SqlCeConnection sqlConnection = new SqlCeConnection(ConnectionString()))
                {
                    using (SqlCeCommand selectCommentCommand = new SqlCeCommand(selectComment, sqlConnection))
                    {
                        SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(selectCommentCommand);

                        selectCommentCommand.Parameters.AddWithValue("@commentID", commentID);

                        // Fill the DataTable with the selected comment from the database.
                        dataAdapter.Fill(comment);
                    }
                }
            }
            catch (SqlCeException ex)
            {
                string error = ex.Message;
            }

            return comment;
        }

        #endregion
    }
}
