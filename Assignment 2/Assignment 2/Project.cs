﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 09/12/2014
// Description: Create, view, and edit Projects.
// Version    : 1.0
//-----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Assignment_2
{
    public class Project
    {
        private string projectTitle;
        private string projectDescription;
        private DateTime projectDeadline;

        private DataAccess data = new DataAccess();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Project()
        {

        }

        /// <summary>
        /// Parameterised constructor for creating a new project to add to the database.
        /// </summary>
        /// <param name="title">A string containing the title of the project.</param>
        /// <param name="description">A string containing a description of the project.</param>
        /// <param name="deadline">A DateTime representing the deadline of the project.</param>
        public Project(string title, string description, DateTime deadline)
        {
            projectTitle = title;
            projectDescription = description;
            projectDeadline = deadline.Date;
        }

        /// <summary>
        /// Creates a new project.
        /// </summary>
        public void CreateProject()
        {
            data.InsertProject(projectTitle, projectDescription, projectDeadline);
        }

        /// <summary>
        /// Updates the project's details.
        /// </summary>
        /// <param name="projectID"></param>
        public void EditProject(int projectID)
        {
            data.UpdateProject(projectTitle, projectDescription, projectDeadline, projectID);
        }

        /// <summary>
        /// Retrieves all projects.
        /// </summary>
        /// <returns>A DataTable containing all projects from the database.</returns>
        public DataTable GetAllProjects()
        {
            DataTable projects = new DataTable();

            projects = data.SelectAllProjects();

            return projects;
        }

        /// <summary>
        /// Retrieves a selected project so the project details can be displayed.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <returns>A DataTable containing a selected project.</returns>
        public DataTable GetProject(int projectID)
        {
            DataTable project = new DataTable();

            project = data.SelectProject(projectID);

            return project;
        }

        /// <summary>
        /// Retrieves a selected project's title.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <returns>A string containing the selected project's title.</returns>
        public string GetProjectTitle(int projectID)
        {
            string projectTitle;

            projectTitle = data.SelectProject(projectID).Rows[0][0].ToString();

            return projectTitle;
        }

        /// <summary>
        /// Retrieves a selected project's description.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <returns>A string containing the selected project's description.</returns>
        public string GetProjectDescription(int projectID)
        {
            string projectDescription;
            
            projectDescription = data.SelectProject(projectID).Rows[0][1].ToString();

            return projectDescription;
        }

        /// <summary>
        /// Retrieves a selected project's deadline.
        /// </summary>
        /// <param name="projectID">The ID of the selected project.</param>
        /// <returns>A DateTime object containing the selected project's deadline.</returns>
        public DateTime GetProjectDeadline(int projectID)
        {
            DateTime projectDeadline;
            
            projectDeadline = (DateTime)data.SelectProject(projectID).Rows[0][2];

            return projectDeadline;
        }

        /// <summary>
        /// Checks that the project details input by the user are valid.
        /// </summary>
        /// <returns>A boolean to indicate whether or not the details valid.</returns>
        public bool CheckNewProjectDetails()
        {
            bool goodInputs = true;

            if (string.IsNullOrWhiteSpace(projectTitle) || string.IsNullOrWhiteSpace(projectDescription) || projectDeadline < DateTime.Today)
            {
                goodInputs = false;
            }

            return goodInputs;
        }
    }
}
