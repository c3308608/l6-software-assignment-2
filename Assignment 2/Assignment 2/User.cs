﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 09/12/2014
// Description: Create and view Users.
// Version    : 1.0
//-----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Assignment_2
{
    public enum UserRole
    {
        Manager,
        Developer
    };

    public abstract class User
    {
        protected string userName;
        protected string userPassword;
        protected int userRole;

        private bool userIsLoggedIn;

        public DataAccess data = new DataAccess();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public User()
        {

        }

        /// <summary>
        /// Parameterised constructor for creating a new user to add to the database.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="role"></param>
        public User(string username, string password, int role)
        {
            userName = username;
            userPassword = password;
            userRole = role;
        }

        /// <summary>
        /// Retrieves all users.
        /// </summary>
        /// <returns>A DataTable containing all users from the database.</returns>
        public static DataTable GetAllUsers()
        {
            DataTable users = new DataTable();

            DataAccess userData = new DataAccess();

            users = userData.SelectAllUsers();

            return users;
        }

        /// <summary>
        /// Confirms whether or not a user exists in the database by checking the supplid username and password.
        /// </summary>
        /// <param name="username">A string containing the username of the user attempting to log in.</param>
        /// <param name="password">A string containing the password of the user attempting to log in.</param>
        /// <param name="userExists">A boolean to indicate whether a user matching the username and password details was found.</param>
        /// <returns></returns>
        public static DataTable Login(string username, string password, out bool userExists)
        {
            DataAccess userData = new DataAccess();

            DataTable loggedInUser = userData.SelectUser(username, password);

            // Check to see if a user matching the supplied username and password was returned from the database.
            if (loggedInUser.Rows.Count > 0)
            {
                userExists = true;
            }
            else
            {
                userExists = false;
            }

            return loggedInUser;
        }

        /// <summary>
        /// Updates the the boolean userIsLoggedIn when the user logs in or out.
        /// </summary>
        /// <param name="loggedIn">A boolean indicating whether or not the user is logged in.</param>
        public void SetLoginStatus(bool loggedIn)
        {
            userIsLoggedIn = loggedIn;
        }

        /// <summary>
        /// Indicates whether or not the user is logged in.
        /// </summary>
        /// <returns></returns>
        public bool GetLoginStatus()
        {
            return userIsLoggedIn;
        }
    }
}
