﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 29/12/2014
// Description: Create, view, and edit Comments.
// Version    : 1.0
//-----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Assignment_2
{
    public class Comment
    {
        private string commentText;
        private int commentTicketID;

        private DataAccess data = new DataAccess();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Comment()
        {

        }

        /// <summary>
        /// Parameterised constructor for creating a new comment to add to the database.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="ticketID"></param>
        public Comment(string text, int ticketID)
        {
            commentText = text;
            commentTicketID = ticketID;
        }

        /// <summary>
        /// Creates a new comment.
        /// </summary>
        public void CreateComment()
        {
            data.InsertComment(commentText, commentTicketID);
        }

        /// <summary>
        /// Retrieves all comments associated with a selected ticket.
        /// </summary>
        /// <param name="ticketID">The ID of the selected ticket.</param>
        /// <returns>A DataTable containing all comments associated with the selected ticket.</returns>
        public DataTable GetTicketComments(int ticketID)
        {
            DataTable ticketComments = new DataTable();

            ticketComments = data.SelectTicketComments(ticketID);

            return ticketComments;
        }

        /// <summary>
        /// Retrieves a selected comment's text.
        /// </summary>
        /// <param name="commentID">The ID of the selected comment.</param>
        /// <returns>A string containing the selected comment's text.</returns>
        public string GetCommentText(int commentID)
        {
            string commentText;

            commentText = data.SelectComment(commentID).Rows[0][0].ToString();

            return commentText;
        }

        /// <summary>
        /// Checks that the comment details input by the user are valid.
        /// </summary>
        /// <returns>A boolean to indicate whether or not the details valid.</returns>
        public bool CheckNewCommentDetails()
        {
            bool goodInputs = true;

            if (string.IsNullOrWhiteSpace(commentText))
            {
                goodInputs = false;
            }

            return goodInputs;
        }
    }
}
