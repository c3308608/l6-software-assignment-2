﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 09/12/2014
// Description: Handles user input and presents data.
// Version    : 1.0
//-----------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Assignment_2;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private Project project = new Project();
        private Ticket ticket = new Ticket();
        private Comment comment = new Comment();
        private Manager manager;
        private Developer developer;

        private bool userloggedIn = false;
        private int? loggedInUserID;

        public Form1()
        {
            InitializeComponent();
        }

        #region Project methods

        private void buttonCreateProject_Click(object sender, EventArgs e)
        {
            // Check that a user is logged in.
            if (userloggedIn == true)
            {
                // Check that the logged in user is a manager - only a manager may create a new project.
                if (manager != null)
                {
                    if (manager.GetLoginStatus() == true)
                    {
                    
                        DialogResult result = MessageBox.Show("Do you want to create project " + textProjectTitle.Text + "?",
                                                              "Create Project", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                        if (result == DialogResult.OK)
                        {
                            Project newProject = new Project(textProjectTitle.Text, textProjectDescription.Text, dateTimeProjectDeadline.Value);

                            // Create the new project if the project details entered are valid.
                            if (newProject.CheckNewProjectDetails())
                            {
                                newProject.CreateProject();
                            }
                            else
                            {
                                MessageBox.Show("The project details entered are invalid.", "Invalid details", 
                                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }

                        // Clear the new project fields.
                        textProjectTitle.Clear();
                        textProjectDescription.Clear();
                        dateTimeProjectDeadline.Value = DateTime.Today;

                        // Update the project list with the new project.
                        fillProjectList();
                    }
                }
                else
                {
                    MessageBox.Show("Only a manager can create a new project.", "Permission denied",
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("You are not logged in.", "Login required",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonShowProjectDetails_Click(object sender, EventArgs e)
        {
            // Display details of a specific project if a project is selected.
            if (listBoxProjectList.SelectedValue != null)
            {
                // Clear any displayed details relating to a project.
                clearTicketDetails();
                labelFilterTickets.Text = "";
                listBoxFilterTickets.DataSource = null;

                int selectedProject = (int)listBoxProjectList.SelectedValue;
                
                // Display the selected project details on the form.
                textSelectedProjectTitle.Text = project.GetProjectTitle(selectedProject);
                textSelectedProjectDescription.Text = project.GetProjectDescription(selectedProject);
                dateTimeSelectedProjectDeadline.Value = project.GetProjectDeadline(selectedProject);
                
                // Repopulate the ticket list.
                listBoxTicketList.DataSource = null;
                listBoxTicketList.DataSource = ticket.GetProjectTickets(selectedProject);
                listBoxTicketList.DisplayMember = "TicketTitle";
                listBoxTicketList.ValueMember = "TicketID";
            }
        }

        private void buttonUpdateProject_Click(object sender, EventArgs e)
        {
            // Check that a user is logged in.
            if (userloggedIn == true)
            {
                // Check that the logged in user is a manager - only a manager may update a project.
                if (manager != null)
                {
                    if (manager.GetLoginStatus() == true)
                    {
                        // A project can only be updated if one is selected from the project list.
                        if (listBoxProjectList.SelectedValue != null)
                        {

                            DialogResult result = MessageBox.Show("Do you want to update this project?",
                                                                  "Update Project", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                            if (result == DialogResult.OK)
                            {
                                int selectedProject = (int)listBoxProjectList.SelectedValue;

                                Project updatedProject = new Project(textSelectedProjectTitle.Text, textSelectedProjectDescription.Text,
                                                                dateTimeSelectedProjectDeadline.Value);

                                // Update the selected project if the updated details entered are valid.
                                if (updatedProject.CheckNewProjectDetails())
                                {
                                    updatedProject.EditProject(selectedProject);
                                }
                                else
                                {
                                    MessageBox.Show("The project details entered are invalid.", "Invalid details",
                                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                            }

                            clearProjectDetails();
                            fillProjectList();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Only a manager can update a project.", "Permission denied",
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("You are not logged in.", "Login required",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region Ticket methods

        private void buttonCreateTicket_Click(object sender, EventArgs e)
        {
            // Check that a user is logged in.
            if (userloggedIn == true)
            {
                // A new ticket can only be created if a project is selected from the project list.
                if (listBoxProjectList.SelectedValue != null)
                {
                    
                    DialogResult result = MessageBox.Show("Create ticket for project " + listBoxProjectList.Text + "?",
                                                          "Create Ticket", MessageBoxButtons.OKCancel);

                    if (result == DialogResult.OK)
                    {
                        int selectedProject = (int)listBoxProjectList.SelectedValue;

                        int ticketPriority = ticket.TicketPriorityInt(comboBoxNewTicketPriority.SelectedValue.ToString());
                        int ticketStatus = ticket.TicketStatusInt(comboBoxNewTicketStatus.SelectedValue.ToString());
                        int ticketType = ticket.TicketTypeInt(comboBoxNewTicketType.SelectedValue.ToString());
                        int ticketOwner = (int)comboBoxNewTicketOwner.SelectedValue;


                        Ticket newTicket = new Ticket(textNewTicketTitle.Text, textNewTicketDescription.Text, ticketPriority,
                                                      ticketStatus, selectedProject, ticketType, ticketOwner);

                        // Create a new ticket for the selected project if the ticket details entered are valid.
                        if (newTicket.CheckNewTicketDetails())
                        {
                            newTicket.CreateTicket();
                        }
                        else
                        {
                            MessageBox.Show("The ticket details entered are invalid.", "Invalid details",
                                             MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }

                    textNewTicketTitle.Text = textNewTicketDescription.Text = "";
                }
            }
            else
            {
                MessageBox.Show("You are not logged in.", "Login required",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonShowTicketDetails_Click(object sender, EventArgs e)
        {
            // Ticket details can only be displayed if a ticket is selected.
            if (listBoxTicketList.SelectedValue != null)
            {
                clearTicketDetails();

                int selectedTicket = (int)listBoxTicketList.SelectedValue;

                // Display the details of the selected ticket.
                showTicket(selectedTicket);
            }
        }

        private void buttonShowFilterTicketDetails_Click(object sender, EventArgs e)
        {
            // Ticket details can only be displayed if a ticket is selected.
            if (listBoxFilterTickets.SelectedValue != null)
            {
                clearTicketDetails();

                int selectedTicket = (int)listBoxFilterTickets.SelectedValue;

                // Display the details of the selected ticket.
                showTicket(selectedTicket);
            }
        }

        private void buttonEditTicket_Click(object sender, EventArgs e)
        {
            // Check that a user is logged in.
            if (userloggedIn == true)
            {
                // Integer to hold the ID of the selected ticket.
                int selectedTicket = 0;

                // Check that a valid ticket ID exists in the textSelectedTicketID field. If it does then it's value will be
                // converted to an integer and assigend to selectedTicket. If there is no valid ticket ID a ticket can not
                // be edited.
                if (Int32.TryParse(textSelectedTicketID.Text, out selectedTicket))
                {

                    DialogResult result = MessageBox.Show("Do you want to update this ticket?",
                                                          "Update Project", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    if (result == DialogResult.OK)
                    {
                        // Get the ID of the project the selected ticket is associated with.
                        int selectedProject = ticket.TicketProjectIDInt(selectedTicket);

                        int ticketPriority = ticket.TicketPriorityInt(comboBoxSelectedTicketPriority.SelectedValue.ToString());
                        int ticketStatus = ticket.TicketStatusInt(comboBoxSelectedTicketStatus.SelectedValue.ToString());
                        int ticketType = ticket.TicketTypeInt(comboBoxSelectedTicketType.SelectedValue.ToString());
                        int ticketOwner = (int)comboBoxSelectedTicketOwner.SelectedValue;

                        Ticket updatedTicket = new Ticket(textSelectedTicketTitle.Text, textSelectedTicketDescription.Text, ticketPriority,
                                                          ticketStatus, selectedProject, ticketType, ticketOwner);

                        // Update the ticket's details if the new ticket details are valid.
                        if (updatedTicket.CheckNewTicketDetails())
                        {
                            updatedTicket.EditTicket(selectedTicket);
                            clearTicketDetails();
                        }
                        else
                        {
                            MessageBox.Show("The ticket details entered are invalid.", "Invalid details",
                                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("You are not logged in.", "Login required",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonFilterOpenTicketType_Click(object sender, EventArgs e)
        {
            // A project must be selected to filter tickets for a project.
            if (listBoxProjectList.SelectedValue != null)
            {
                int selectedProject = (int)listBoxProjectList.SelectedValue;

                int ticketType = ticket.TicketTypeInt(comboBoxFilterOpenTicketType.SelectedValue.ToString());

                int ticketStatus = (int)TicketStatus.Open;

                // Populate the filtered tickets listbox with all tickets matching the filter criteria.
                listBoxFilterTickets.DataSource = null;
                listBoxFilterTickets.DataSource = ticket.GetProjectTicketsByTypeAndStatus(selectedProject, ticketType, ticketStatus);
                listBoxFilterTickets.DisplayMember = "TicketTitle";
                listBoxFilterTickets.ValueMember = "TicketID";

                // Update the label to show the filtering criteria.
                labelFilterTickets.Text = "Open " + comboBoxFilterOpenTicketType.SelectedValue.ToString() + " tickets for "
                                                  + project.GetProjectTitle(selectedProject) + ".";
            }
        }

        private void buttonFilterOpenTicketPriority_Click(object sender, EventArgs e)
        {
            // A project must be selected to filter tickets for a project.
            if (listBoxProjectList.SelectedValue != null)
            {
                int selectedProject = (int)listBoxProjectList.SelectedValue;

                int ticketPriority = ticket.TicketPriorityInt(comboBoxFilterOpenTicketPriority.SelectedValue.ToString());

                int ticketStatus = (int)TicketStatus.Open;

                // Populate the filtered tickets listbox with all tickets matching the filter criteria.
                listBoxFilterTickets.DataSource = null;
                listBoxFilterTickets.DataSource = ticket.GetProjectTicketsByPriorityAndStatus(selectedProject, ticketPriority, ticketStatus);
                listBoxFilterTickets.DisplayMember = "TicketTitle";
                listBoxFilterTickets.ValueMember = "TicketID";

                // Update the label to show the filtering criteria.
                labelFilterTickets.Text = "Open " + comboBoxFilterOpenTicketPriority.SelectedValue.ToString() + " priority tickets for "
                                                  + project.GetProjectTitle(selectedProject) + ".";
            }
        }

        private void buttonFilterWaitingTicketType_Click(object sender, EventArgs e)
        {
            // A project must be selected to filter tickets for a project.
            if (listBoxProjectList.SelectedValue != null)
            {
                int selectedProject = (int)listBoxProjectList.SelectedValue;

                int ticketType = ticket.TicketTypeInt(comboBoxFilterWaitingTicketType.SelectedValue.ToString());

                int ticketStatus = (int)TicketStatus.Waiting;

                // Populate the filtered tickets listbox with all tickets matching the filter criteria.
                listBoxFilterTickets.DataSource = null;
                listBoxFilterTickets.DataSource = ticket.GetProjectTicketsByTypeAndStatus(selectedProject, ticketType, ticketStatus);
                listBoxFilterTickets.DisplayMember = "TicketTitle";
                listBoxFilterTickets.ValueMember = "TicketID";

                // Update the label to show the filtering criteria.
                labelFilterTickets.Text = "Waiting " + comboBoxFilterWaitingTicketType.SelectedValue.ToString() + " tickets for "
                                                     + project.GetProjectTitle(selectedProject) + ".";
            }
        }

        private void buttonFilterWaitingTicketPriority_Click(object sender, EventArgs e)
        {
            // A project must be selected to filter tickets for a project.
            if (listBoxProjectList.SelectedValue != null)
            {
                int selectedProject = (int)listBoxProjectList.SelectedValue;

                int ticketPriority = ticket.TicketPriorityInt(comboBoxFilterWaitingTicketPriority.SelectedValue.ToString());

                int ticketStatus = (int)TicketStatus.Waiting;

                // Populate the filtered tickets listbox with all tickets matching the filter criteria.
                listBoxFilterTickets.DataSource = null;
                listBoxFilterTickets.DataSource = ticket.GetProjectTicketsByPriorityAndStatus(selectedProject, ticketPriority, ticketStatus);
                listBoxFilterTickets.DisplayMember = "TicketTitle";
                listBoxFilterTickets.ValueMember = "TicketID";

                // Update the label to show the filtering criteria.
                labelFilterTickets.Text = "Waiting " + comboBoxFilterWaitingTicketPriority.SelectedValue.ToString() + " priority tickets for "
                                                     + project.GetProjectTitle(selectedProject) + ".";
            }
        }

        private void buttonFilterUserOpenTickets_Click(object sender, EventArgs e)
        {
            // A user must be selected to filter tickets by user.
            if (comboBoxFilterUserOpenTickets.SelectedValue != null)
            {
                int ticketOwner = (int)comboBoxFilterUserOpenTickets.SelectedValue;

                int ticketStatus = (int)TicketStatus.Open;

                // Populate the filtered tickets listbox with all tickets matching the filter criteria.
                listBoxFilterTickets.DataSource = null;
                listBoxFilterTickets.DataSource = ticket.GetTicketsByUserAndStatus(ticketOwner, ticketStatus);
                listBoxFilterTickets.DisplayMember = "TicketTitle";
                listBoxFilterTickets.ValueMember = "TicketID";

                // Update the label to show the filtering criteria.
                labelFilterTickets.Text = comboBoxFilterUserOpenTickets.Text + " open tickets.";
            }
        }

        private void buttonFilterUserWaitingTickets_Click(object sender, EventArgs e)
        {
            // A user must be selected to filter tickets by user.
            if (comboBoxFilterUserOpenTickets.SelectedValue != null)
            {
                int ticketOwner = (int)comboBoxFilterUserWaitingTickets.SelectedValue;

                int ticketStatus = (int)TicketStatus.Waiting;

                // Populate the filtered tickets listbox with all tickets matching the filter criteria.
                listBoxFilterTickets.DataSource = null;
                listBoxFilterTickets.DataSource = ticket.GetTicketsByUserAndStatus(ticketOwner, ticketStatus);
                listBoxFilterTickets.DisplayMember = "TicketTitle";
                listBoxFilterTickets.ValueMember = "TicketID";

                // Update the label to show the filtering criteria.
                labelFilterTickets.Text = comboBoxFilterUserWaitingTickets.Text + " waiting tickets.";
            }
        }

        #endregion

        #region Comment methods

        private void buttonAddComment_Click(object sender, EventArgs e)
        {
            // Check that a user is logged in.
            if (userloggedIn == true)
            {
                int selectedTicket = 0;

                // Check that a valid ticket ID exists in the textSelectedTicketID field. If it does then it's value will be
                // converted to an integer and assigend to selectedTicket. If there is no valid ticket ID a comment can not be
                // added to a ticket.
                if (Int32.TryParse(textSelectedTicketID.Text, out selectedTicket))
                {

                    DialogResult result = MessageBox.Show("Add comment for ticket " + ticket.TicketTitleString(selectedTicket) + "?",
                                                          "Add Ticket", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    if (result == DialogResult.OK)
                    {
                        Comment newComment = new Comment(textAddComment.Text, selectedTicket);

                        // Add a new comment to the selected ticket if the comment details entered are valid.
                        if (newComment.CheckNewCommentDetails())
                        {
                            newComment.CreateComment();

                            // Repopulate the list of comments so the new comment can be seen in the list.
                            listBoxCommentList.DataSource = null;
                            listBoxCommentList.DataSource = comment.GetTicketComments(selectedTicket);
                            listBoxCommentList.DisplayMember = "CommentID";
                            listBoxCommentList.ValueMember = "CommentID";
                        }
                        else
                        {
                            MessageBox.Show("The comment details entered are invalid.", "Invalid details",
                                             MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }

                    textAddComment.Clear();
                    textViewComment.Clear();
                }
            }
            else
            {
                MessageBox.Show("You are not logged in.", "Login required",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonViewComment_Click(object sender, EventArgs e)
        {
            // A comment must be selected to view a comment.
            if (listBoxCommentList.SelectedValue != null)
            {
                int selectedComment = (int)listBoxCommentList.SelectedValue;

                // Display the selected comment.
                textViewComment.Text = comment.GetCommentText(selectedComment);
            }
        }

        #endregion

        #region User methods

        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            // Check that a user is logged in.
            if (userloggedIn == true)
            {
                // Check that the logged in user is a manager - only a manager may add a new user.
                if (manager != null)
                {
                    if (manager.GetLoginStatus() == true)
                    {

                        DialogResult result = MessageBox.Show("Do you want to create user " + textNewUsername.Text + "?",
                                                              "Create User", MessageBoxButtons.OKCancel);

                        if (result == DialogResult.OK)
                        {
                            int newUserRole = manager.SetUserRole(comboBoxNewUserRole.SelectedValue.ToString());

                            // A boolean value to indicate whether the new user was created successfully.
                            bool userCreated = manager.CreateUser(textNewUsername.Text, textNewUserPassword.Text, newUserRole);

                            if (userCreated)
                            {
                                MessageBox.Show("New user " + textNewUsername.Text + " created.");

                                // Update all comboboxes that contain usernames so the new user is included.
                                comboBoxNewTicketOwner.DataSource = User.GetAllUsers();
                                comboBoxNewTicketOwner.DisplayMember = "Username";
                                comboBoxNewTicketOwner.ValueMember = "UserID";

                                comboBoxFilterUserOpenTickets.DataSource = User.GetAllUsers();
                                comboBoxFilterUserOpenTickets.DisplayMember = "Username";
                                comboBoxFilterUserOpenTickets.ValueMember = "UserID";

                                comboBoxFilterUserWaitingTickets.DataSource = User.GetAllUsers();
                                comboBoxFilterUserWaitingTickets.DisplayMember = "Username";
                                comboBoxFilterUserWaitingTickets.ValueMember = "UserID";
                            }
                            else
                            {
                                MessageBox.Show("The user details entered are invalid.");
                            }
                        }

                        textNewUsername.Text = textNewUserPassword.Text = "";
                    }
                }
                else
                {
                    MessageBox.Show("Only a manager can create a new user.", "Permission denied",
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("You are not logged in.", "Login required",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region Log in/out methods

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            // Checks that a user is not already logged in.
            if (userloggedIn == false)
            {
                // Use the supplied username and password to check if the user exists.
                bool userExists;
                DataTable loggedInUser = User.Login(textUsername.Text, textPassword.Text, out userExists);

                if (userExists == true)
                {
                    // Set userloggedIn variable to show that a user is logged in.
                    userloggedIn = true;

                    loggedInUserID = (int)loggedInUser.Rows[0][0];

                    int userRoleInt = (int)loggedInUser.Rows[0][3];
                    string userRoleString;

                    UserRole userRoleEnum = (UserRole)userRoleInt;
                    userRoleString = userRoleEnum.ToString();

                    // Check the user's role and instantiate the appropriate object.
                    if (userRoleEnum == UserRole.Manager)
                    {
                        manager = new Manager();
                        manager.SetLoginStatus(true);
                    }
                    if (userRoleEnum == UserRole.Developer)
                    {
                        developer = new Developer();
                        developer.SetLoginStatus(true);
                    }

                    // Display information about the logged in user.
                    labelLoggedInUsername.Text = "Logged in user:\r\n" + textUsername.Text;
                    labelLoggedInUserRole.Text = "Logged in user role :\r\n"+ userRoleString;

                    initialiseFields();

                    textUsername.Text = textPassword.Text = "";
                }
                else
                {
                    MessageBox.Show("The login details entered were invald.", "Login Fail", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    textUsername.Text = textPassword.Text = "";
                }
            }
            else
            {
                MessageBox.Show("User already logged in.", "Logged In", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textUsername.Text = textPassword.Text = "";
            }
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            userloggedIn = false;

            if (manager != null)
            {
                manager.SetLoginStatus(false);
                manager = null;
            }

            if (developer != null)
            {
                developer.SetLoginStatus(false);
                developer = null;
            }

            clearAllFields();

            loggedInUserID = null;

            labelLoggedInUsername.Text = "Logged Out";
            labelLoggedInUserRole.Text = "";
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Initialises all of the forms combo boxes and the project list.
        /// </summary>
        private void initialiseFields()
        {
            comboBoxNewTicketPriority.DataSource = Enum.GetValues(typeof(TicketPriority));
            comboBoxNewTicketStatus.DataSource = Enum.GetValues(typeof(TicketStatus));
            comboBoxNewTicketType.DataSource = Enum.GetValues(typeof(TicketType));

            comboBoxNewTicketOwner.DataSource = User.GetAllUsers();
            comboBoxNewTicketOwner.DisplayMember = "Username";
            comboBoxNewTicketOwner.ValueMember = "UserID";

            comboBoxNewUserRole.DataSource = Enum.GetValues(typeof(UserRole));

            comboBoxFilterOpenTicketType.DataSource = Enum.GetValues(typeof(TicketType));
            comboBoxFilterOpenTicketPriority.DataSource = Enum.GetValues(typeof(TicketPriority));
            comboBoxFilterWaitingTicketType.DataSource = Enum.GetValues(typeof(TicketType));
            comboBoxFilterWaitingTicketPriority.DataSource = Enum.GetValues(typeof(TicketPriority));

            comboBoxFilterUserOpenTickets.DataSource = User.GetAllUsers();
            comboBoxFilterUserOpenTickets.DisplayMember = "Username";
            comboBoxFilterUserOpenTickets.ValueMember = "UserID";

            comboBoxFilterUserWaitingTickets.DataSource = User.GetAllUsers();
            comboBoxFilterUserWaitingTickets.DisplayMember = "Username";
            comboBoxFilterUserWaitingTickets.ValueMember = "UserID";

            fillProjectList();
        }

        /// <summary>
        /// Clears all the fields on the form.
        /// </summary>
        private void clearAllFields()
        {
            clearProjectDetails();
            clearTicketDetails();
            clearNewTicketDetails();
            clearNewUserDetails();
            clearFilterTicketDetails();

            listBoxTicketList.DataSource = null;
        }

        /// <summary>
        /// Fills the project list with all projects that exist in the database.
        /// </summary>
        private void fillProjectList()
        {
            listBoxProjectList.DataSource = project.GetAllProjects();
            listBoxProjectList.DisplayMember = "ProjectTitle";
            listBoxProjectList.ValueMember = "ProjectID";
        }

        /// <summary>
        /// Clears all information about projects from the form.
        /// </summary>
        private void clearProjectDetails()
        {
            listBoxProjectList.DataSource = null;
            textProjectTitle.Text = textProjectDescription.Text = textSelectedProjectDescription.Text = textSelectedProjectTitle.Text = "";
            dateTimeSelectedProjectDeadline.Value = DateTime.Today;
        }

        /// <summary>
        /// Clears all information about the selected ticket from the form.
        /// </summary>
        private void clearTicketDetails()
        {
            listBoxCommentList.DataSource = null;

            textSelectedTicketProjectTitle.Text = textSelectedTicketID.Text = textSelectedTicketTitle.Text = 
            textSelectedTicketDescription.Text = "";

            comboBoxSelectedTicketOwner.DataSource = null;
            comboBoxSelectedTicketType.DataSource = null;
            comboBoxSelectedTicketPriority.DataSource = null;
            comboBoxSelectedTicketStatus.DataSource = null;

            textAddComment.Text = textViewComment.Text = "";
        }

        /// <summary>
        /// Clears all details in the new ticket fields.
        /// </summary>
        private void clearNewTicketDetails()
        {
            textNewTicketTitle.Text = textNewTicketDescription.Text = "";
            comboBoxNewTicketOwner.DataSource = null;
            comboBoxNewTicketType.DataSource = null;
            comboBoxNewTicketPriority.DataSource = null;
            comboBoxNewTicketStatus.DataSource = null;
        }

        /// <summary>
        /// Clears all details in the new user fields.
        /// </summary>
        private void clearNewUserDetails()
        {
            textNewUsername.Text = textNewUserPassword.Text = "";
            comboBoxNewUserRole.DataSource = null;
        }

        /// <summary>
        /// Clears all values from the filter ticket fields.
        /// </summary>
        private void clearFilterTicketDetails()
        {
            comboBoxFilterOpenTicketType.DataSource = null;
            comboBoxFilterWaitingTicketType.DataSource = null;
            comboBoxFilterOpenTicketPriority.DataSource = null;
            comboBoxFilterWaitingTicketPriority.DataSource = null;
            comboBoxFilterUserOpenTickets.DataSource = null;
            comboBoxFilterUserWaitingTickets.DataSource = null;
            listBoxFilterTickets.DataSource = null;
            labelFilterTickets.Text = "";
        }

        /// <summary>
        /// Displays details of the selected ticket.
        /// </summary>
        /// <param name="selectedTicket">An integer representing the ID of the selected ticket.</param>
        private void showTicket(int selectedTicket)
        {
            textSelectedTicketProjectTitle.Text = ticket.TicketProjectTitleString(selectedTicket);
            textSelectedTicketID.Text = selectedTicket.ToString();
            textSelectedTicketTitle.Text = ticket.TicketTitleString(selectedTicket);
            textSelectedTicketDescription.Text = ticket.TicketDescriptionString(selectedTicket);

            comboBoxSelectedTicketOwner.DataSource = User.GetAllUsers();
            comboBoxSelectedTicketOwner.DisplayMember = "Username";
            comboBoxSelectedTicketOwner.ValueMember = "UserID";
            comboBoxSelectedTicketOwner.SelectedIndex = comboBoxSelectedTicketOwner.FindStringExact(ticket.TicketOwnerString(selectedTicket));

            comboBoxSelectedTicketType.DataSource = Enum.GetValues(typeof(TicketType));
            comboBoxSelectedTicketType.SelectedIndex = ticket.TicketTypeInt(ticket.TicketTypeString(selectedTicket));
            comboBoxSelectedTicketType.Visible = true;
            comboBoxSelectedTicketPriority.DataSource = Enum.GetValues(typeof(TicketPriority));
            comboBoxSelectedTicketPriority.SelectedIndex = ticket.TicketPriorityInt(ticket.TicketPriorityString(selectedTicket));
            comboBoxSelectedTicketStatus.DataSource = Enum.GetValues(typeof(TicketStatus));
            comboBoxSelectedTicketStatus.SelectedIndex = ticket.TicketStatusInt(ticket.TicketStatusString(selectedTicket));

            listBoxCommentList.DataSource = null;
            listBoxCommentList.DataSource = comment.GetTicketComments(selectedTicket);
            listBoxCommentList.DisplayMember = "CommentID";
            listBoxCommentList.ValueMember = "CommentID";
        }

        #endregion
    }
}
