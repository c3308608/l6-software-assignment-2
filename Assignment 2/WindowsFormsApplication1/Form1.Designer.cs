﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textProjectTitle = new System.Windows.Forms.TextBox();
            this.textProjectDescription = new System.Windows.Forms.TextBox();
            this.buttonCreateProject = new System.Windows.Forms.Button();
            this.dateTimeProjectDeadline = new System.Windows.Forms.DateTimePicker();
            this.listBoxProjectList = new System.Windows.Forms.ListBox();
            this.labelNewProjectTitle = new System.Windows.Forms.Label();
            this.labelNewProjectDescription = new System.Windows.Forms.Label();
            this.labelNewProjectDeadline = new System.Windows.Forms.Label();
            this.labelProjectList = new System.Windows.Forms.Label();
            this.textSelectedProjectTitle = new System.Windows.Forms.TextBox();
            this.textSelectedProjectDescription = new System.Windows.Forms.TextBox();
            this.buttonShowProjectDetails = new System.Windows.Forms.Button();
            this.labelProjectTitle = new System.Windows.Forms.Label();
            this.labelProjectDescription = new System.Windows.Forms.Label();
            this.labelProjectDeadline = new System.Windows.Forms.Label();
            this.listBoxTicketList = new System.Windows.Forms.ListBox();
            this.textNewTicketTitle = new System.Windows.Forms.TextBox();
            this.textNewTicketDescription = new System.Windows.Forms.TextBox();
            this.labelNewTicketTitle = new System.Windows.Forms.Label();
            this.labelNewTicketDescription = new System.Windows.Forms.Label();
            this.labelNewTicketPriority = new System.Windows.Forms.Label();
            this.labelNewTicketStatus = new System.Windows.Forms.Label();
            this.labelNewTicketType = new System.Windows.Forms.Label();
            this.buttonCreateTicket = new System.Windows.Forms.Button();
            this.comboBoxNewTicketPriority = new System.Windows.Forms.ComboBox();
            this.comboBoxNewTicketType = new System.Windows.Forms.ComboBox();
            this.comboBoxNewTicketStatus = new System.Windows.Forms.ComboBox();
            this.labelProjectTickets = new System.Windows.Forms.Label();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.comboBoxNewTicketOwner = new System.Windows.Forms.ComboBox();
            this.labelNewTicketOwner = new System.Windows.Forms.Label();
            this.textSelectedTicketProjectTitle = new System.Windows.Forms.TextBox();
            this.labelTicketProjectTitle = new System.Windows.Forms.Label();
            this.textSelectedTicketTitle = new System.Windows.Forms.TextBox();
            this.textSelectedTicketDescription = new System.Windows.Forms.TextBox();
            this.labelTicketTitle = new System.Windows.Forms.Label();
            this.labelTicketDescription = new System.Windows.Forms.Label();
            this.labelTicketOwner = new System.Windows.Forms.Label();
            this.labelTicketType = new System.Windows.Forms.Label();
            this.labelTicketPriority = new System.Windows.Forms.Label();
            this.labelTicketStatus = new System.Windows.Forms.Label();
            this.buttonShowTicketDetails = new System.Windows.Forms.Button();
            this.buttonAddComment = new System.Windows.Forms.Button();
            this.textAddComment = new System.Windows.Forms.TextBox();
            this.labelAddComment = new System.Windows.Forms.Label();
            this.listBoxCommentList = new System.Windows.Forms.ListBox();
            this.labelCommentList = new System.Windows.Forms.Label();
            this.textViewComment = new System.Windows.Forms.TextBox();
            this.labelViewComment = new System.Windows.Forms.Label();
            this.buttonViewComment = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.labelLoggedInUsername = new System.Windows.Forms.Label();
            this.labelLoggedInUserRole = new System.Windows.Forms.Label();
            this.buttonAddUser = new System.Windows.Forms.Button();
            this.textNewUsername = new System.Windows.Forms.TextBox();
            this.textNewUserPassword = new System.Windows.Forms.TextBox();
            this.comboBoxNewUserRole = new System.Windows.Forms.ComboBox();
            this.labelNewUsername = new System.Windows.Forms.Label();
            this.labelNewUserPassword = new System.Windows.Forms.Label();
            this.labelNewUserRole = new System.Windows.Forms.Label();
            this.dateTimeSelectedProjectDeadline = new System.Windows.Forms.DateTimePicker();
            this.buttonUpdateProject = new System.Windows.Forms.Button();
            this.comboBoxSelectedTicketType = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectedTicketPriority = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectedTicketStatus = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectedTicketOwner = new System.Windows.Forms.ComboBox();
            this.buttonEditTicket = new System.Windows.Forms.Button();
            this.listBoxFilterTickets = new System.Windows.Forms.ListBox();
            this.comboBoxFilterOpenTicketType = new System.Windows.Forms.ComboBox();
            this.labelFilterOpenTicketType = new System.Windows.Forms.Label();
            this.buttonFilterOpenTicketType = new System.Windows.Forms.Button();
            this.comboBoxFilterOpenTicketPriority = new System.Windows.Forms.ComboBox();
            this.labelFilterOpenTicketPriority = new System.Windows.Forms.Label();
            this.buttonFilterOpenTicketPriority = new System.Windows.Forms.Button();
            this.labelFilterTickets = new System.Windows.Forms.Label();
            this.comboBoxFilterWaitingTicketType = new System.Windows.Forms.ComboBox();
            this.labelFilterWaitingTicketType = new System.Windows.Forms.Label();
            this.labelFilterWaitingTicketPriority = new System.Windows.Forms.Label();
            this.comboBoxFilterWaitingTicketPriority = new System.Windows.Forms.ComboBox();
            this.buttonFilterWaitingTicketType = new System.Windows.Forms.Button();
            this.buttonFilterWaitingTicketPriority = new System.Windows.Forms.Button();
            this.comboBoxFilterUserOpenTickets = new System.Windows.Forms.ComboBox();
            this.labelFilterUserOpenTickets = new System.Windows.Forms.Label();
            this.buttonFilterUserOpenTickets = new System.Windows.Forms.Button();
            this.comboBoxFilterUserWaitingTickets = new System.Windows.Forms.ComboBox();
            this.labelFilterUserWaitingTickets = new System.Windows.Forms.Label();
            this.buttonFilterUserWaitingTickets = new System.Windows.Forms.Button();
            this.buttonShowFilterTicketDetails = new System.Windows.Forms.Button();
            this.textSelectedTicketID = new System.Windows.Forms.TextBox();
            this.labelTicketID = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textProjectTitle
            // 
            this.textProjectTitle.Location = new System.Drawing.Point(12, 86);
            this.textProjectTitle.Name = "textProjectTitle";
            this.textProjectTitle.Size = new System.Drawing.Size(115, 20);
            this.textProjectTitle.TabIndex = 5;
            // 
            // textProjectDescription
            // 
            this.textProjectDescription.Location = new System.Drawing.Point(12, 123);
            this.textProjectDescription.Name = "textProjectDescription";
            this.textProjectDescription.Size = new System.Drawing.Size(115, 20);
            this.textProjectDescription.TabIndex = 7;
            // 
            // buttonCreateProject
            // 
            this.buttonCreateProject.Location = new System.Drawing.Point(12, 194);
            this.buttonCreateProject.Name = "buttonCreateProject";
            this.buttonCreateProject.Size = new System.Drawing.Size(115, 23);
            this.buttonCreateProject.TabIndex = 10;
            this.buttonCreateProject.Text = "Create Project";
            this.buttonCreateProject.UseVisualStyleBackColor = true;
            this.buttonCreateProject.Click += new System.EventHandler(this.buttonCreateProject_Click);
            // 
            // dateTimeProjectDeadline
            // 
            this.dateTimeProjectDeadline.Location = new System.Drawing.Point(12, 160);
            this.dateTimeProjectDeadline.Name = "dateTimeProjectDeadline";
            this.dateTimeProjectDeadline.Size = new System.Drawing.Size(115, 20);
            this.dateTimeProjectDeadline.TabIndex = 9;
            // 
            // listBoxProjectList
            // 
            this.listBoxProjectList.FormattingEnabled = true;
            this.listBoxProjectList.Location = new System.Drawing.Point(133, 86);
            this.listBoxProjectList.Name = "listBoxProjectList";
            this.listBoxProjectList.Size = new System.Drawing.Size(197, 95);
            this.listBoxProjectList.TabIndex = 12;
            // 
            // labelNewProjectTitle
            // 
            this.labelNewProjectTitle.AutoSize = true;
            this.labelNewProjectTitle.Location = new System.Drawing.Point(9, 70);
            this.labelNewProjectTitle.Name = "labelNewProjectTitle";
            this.labelNewProjectTitle.Size = new System.Drawing.Size(88, 13);
            this.labelNewProjectTitle.TabIndex = 4;
            this.labelNewProjectTitle.Text = "New Project Title";
            // 
            // labelNewProjectDescription
            // 
            this.labelNewProjectDescription.AutoSize = true;
            this.labelNewProjectDescription.Location = new System.Drawing.Point(9, 107);
            this.labelNewProjectDescription.Name = "labelNewProjectDescription";
            this.labelNewProjectDescription.Size = new System.Drawing.Size(121, 13);
            this.labelNewProjectDescription.TabIndex = 6;
            this.labelNewProjectDescription.Text = "New Project Description";
            // 
            // labelNewProjectDeadline
            // 
            this.labelNewProjectDeadline.AutoSize = true;
            this.labelNewProjectDeadline.Location = new System.Drawing.Point(9, 144);
            this.labelNewProjectDeadline.Name = "labelNewProjectDeadline";
            this.labelNewProjectDeadline.Size = new System.Drawing.Size(110, 13);
            this.labelNewProjectDeadline.TabIndex = 8;
            this.labelNewProjectDeadline.Text = "New Project Deadline";
            // 
            // labelProjectList
            // 
            this.labelProjectList.AutoSize = true;
            this.labelProjectList.Location = new System.Drawing.Point(130, 70);
            this.labelProjectList.Name = "labelProjectList";
            this.labelProjectList.Size = new System.Drawing.Size(59, 13);
            this.labelProjectList.TabIndex = 11;
            this.labelProjectList.Text = "Project List";
            // 
            // textSelectedProjectTitle
            // 
            this.textSelectedProjectTitle.Location = new System.Drawing.Point(336, 86);
            this.textSelectedProjectTitle.Name = "textSelectedProjectTitle";
            this.textSelectedProjectTitle.Size = new System.Drawing.Size(115, 20);
            this.textSelectedProjectTitle.TabIndex = 15;
            // 
            // textSelectedProjectDescription
            // 
            this.textSelectedProjectDescription.Location = new System.Drawing.Point(336, 123);
            this.textSelectedProjectDescription.Name = "textSelectedProjectDescription";
            this.textSelectedProjectDescription.Size = new System.Drawing.Size(115, 20);
            this.textSelectedProjectDescription.TabIndex = 17;
            // 
            // buttonShowProjectDetails
            // 
            this.buttonShowProjectDetails.Location = new System.Drawing.Point(133, 194);
            this.buttonShowProjectDetails.Name = "buttonShowProjectDetails";
            this.buttonShowProjectDetails.Size = new System.Drawing.Size(197, 23);
            this.buttonShowProjectDetails.TabIndex = 13;
            this.buttonShowProjectDetails.Text = "Show Project Details";
            this.buttonShowProjectDetails.UseVisualStyleBackColor = true;
            this.buttonShowProjectDetails.Click += new System.EventHandler(this.buttonShowProjectDetails_Click);
            // 
            // labelProjectTitle
            // 
            this.labelProjectTitle.AutoSize = true;
            this.labelProjectTitle.Location = new System.Drawing.Point(333, 70);
            this.labelProjectTitle.Name = "labelProjectTitle";
            this.labelProjectTitle.Size = new System.Drawing.Size(63, 13);
            this.labelProjectTitle.TabIndex = 14;
            this.labelProjectTitle.Text = "Project Title";
            // 
            // labelProjectDescription
            // 
            this.labelProjectDescription.AutoSize = true;
            this.labelProjectDescription.Location = new System.Drawing.Point(333, 107);
            this.labelProjectDescription.Name = "labelProjectDescription";
            this.labelProjectDescription.Size = new System.Drawing.Size(96, 13);
            this.labelProjectDescription.TabIndex = 16;
            this.labelProjectDescription.Text = "Project Description";
            // 
            // labelProjectDeadline
            // 
            this.labelProjectDeadline.AutoSize = true;
            this.labelProjectDeadline.Location = new System.Drawing.Point(333, 144);
            this.labelProjectDeadline.Name = "labelProjectDeadline";
            this.labelProjectDeadline.Size = new System.Drawing.Size(85, 13);
            this.labelProjectDeadline.TabIndex = 18;
            this.labelProjectDeadline.Text = "Project Deadline";
            // 
            // listBoxTicketList
            // 
            this.listBoxTicketList.FormattingEnabled = true;
            this.listBoxTicketList.Location = new System.Drawing.Point(459, 86);
            this.listBoxTicketList.Name = "listBoxTicketList";
            this.listBoxTicketList.Size = new System.Drawing.Size(120, 95);
            this.listBoxTicketList.TabIndex = 21;
            // 
            // textNewTicketTitle
            // 
            this.textNewTicketTitle.Location = new System.Drawing.Point(12, 248);
            this.textNewTicketTitle.Name = "textNewTicketTitle";
            this.textNewTicketTitle.Size = new System.Drawing.Size(115, 20);
            this.textNewTicketTitle.TabIndex = 18;
            // 
            // textNewTicketDescription
            // 
            this.textNewTicketDescription.Location = new System.Drawing.Point(12, 285);
            this.textNewTicketDescription.Multiline = true;
            this.textNewTicketDescription.Name = "textNewTicketDescription";
            this.textNewTicketDescription.Size = new System.Drawing.Size(115, 60);
            this.textNewTicketDescription.TabIndex = 19;
            // 
            // labelNewTicketTitle
            // 
            this.labelNewTicketTitle.AutoSize = true;
            this.labelNewTicketTitle.Location = new System.Drawing.Point(9, 233);
            this.labelNewTicketTitle.Name = "labelNewTicketTitle";
            this.labelNewTicketTitle.Size = new System.Drawing.Size(85, 13);
            this.labelNewTicketTitle.TabIndex = 20;
            this.labelNewTicketTitle.Text = "New Ticket Title";
            // 
            // labelNewTicketDescription
            // 
            this.labelNewTicketDescription.AutoSize = true;
            this.labelNewTicketDescription.Location = new System.Drawing.Point(9, 270);
            this.labelNewTicketDescription.Name = "labelNewTicketDescription";
            this.labelNewTicketDescription.Size = new System.Drawing.Size(118, 13);
            this.labelNewTicketDescription.TabIndex = 21;
            this.labelNewTicketDescription.Text = "New Ticket Description";
            // 
            // labelNewTicketPriority
            // 
            this.labelNewTicketPriority.AutoSize = true;
            this.labelNewTicketPriority.Location = new System.Drawing.Point(130, 308);
            this.labelNewTicketPriority.Name = "labelNewTicketPriority";
            this.labelNewTicketPriority.Size = new System.Drawing.Size(96, 13);
            this.labelNewTicketPriority.TabIndex = 25;
            this.labelNewTicketPriority.Text = "New Ticket Priority";
            // 
            // labelNewTicketStatus
            // 
            this.labelNewTicketStatus.AutoSize = true;
            this.labelNewTicketStatus.Location = new System.Drawing.Point(130, 347);
            this.labelNewTicketStatus.Name = "labelNewTicketStatus";
            this.labelNewTicketStatus.Size = new System.Drawing.Size(95, 13);
            this.labelNewTicketStatus.TabIndex = 26;
            this.labelNewTicketStatus.Text = "New Ticket Status";
            // 
            // labelNewTicketType
            // 
            this.labelNewTicketType.AutoSize = true;
            this.labelNewTicketType.Location = new System.Drawing.Point(130, 271);
            this.labelNewTicketType.Name = "labelNewTicketType";
            this.labelNewTicketType.Size = new System.Drawing.Size(89, 13);
            this.labelNewTicketType.TabIndex = 27;
            this.labelNewTicketType.Text = "New Ticket Type";
            // 
            // buttonCreateTicket
            // 
            this.buttonCreateTicket.Location = new System.Drawing.Point(12, 360);
            this.buttonCreateTicket.Name = "buttonCreateTicket";
            this.buttonCreateTicket.Size = new System.Drawing.Size(115, 23);
            this.buttonCreateTicket.TabIndex = 28;
            this.buttonCreateTicket.Text = "Create Ticket";
            this.buttonCreateTicket.UseVisualStyleBackColor = true;
            this.buttonCreateTicket.Click += new System.EventHandler(this.buttonCreateTicket_Click);
            // 
            // comboBoxNewTicketPriority
            // 
            this.comboBoxNewTicketPriority.FormattingEnabled = true;
            this.comboBoxNewTicketPriority.Location = new System.Drawing.Point(133, 323);
            this.comboBoxNewTicketPriority.Name = "comboBoxNewTicketPriority";
            this.comboBoxNewTicketPriority.Size = new System.Drawing.Size(120, 21);
            this.comboBoxNewTicketPriority.TabIndex = 29;
            // 
            // comboBoxNewTicketType
            // 
            this.comboBoxNewTicketType.FormattingEnabled = true;
            this.comboBoxNewTicketType.Location = new System.Drawing.Point(133, 286);
            this.comboBoxNewTicketType.Name = "comboBoxNewTicketType";
            this.comboBoxNewTicketType.Size = new System.Drawing.Size(120, 21);
            this.comboBoxNewTicketType.TabIndex = 30;
            // 
            // comboBoxNewTicketStatus
            // 
            this.comboBoxNewTicketStatus.FormattingEnabled = true;
            this.comboBoxNewTicketStatus.Location = new System.Drawing.Point(133, 362);
            this.comboBoxNewTicketStatus.Name = "comboBoxNewTicketStatus";
            this.comboBoxNewTicketStatus.Size = new System.Drawing.Size(120, 21);
            this.comboBoxNewTicketStatus.TabIndex = 31;
            // 
            // labelProjectTickets
            // 
            this.labelProjectTickets.AutoSize = true;
            this.labelProjectTickets.Location = new System.Drawing.Point(456, 70);
            this.labelProjectTickets.Name = "labelProjectTickets";
            this.labelProjectTickets.Size = new System.Drawing.Size(78, 13);
            this.labelProjectTickets.TabIndex = 20;
            this.labelProjectTickets.Text = "Project Tickets";
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(12, 18);
            this.textUsername.MaxLength = 8;
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(100, 20);
            this.textUsername.TabIndex = 0;
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(118, 18);
            this.textPassword.MaxLength = 8;
            this.textPassword.Name = "textPassword";
            this.textPassword.PasswordChar = 'x';
            this.textPassword.Size = new System.Drawing.Size(100, 20);
            this.textPassword.TabIndex = 1;
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(9, 2);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(55, 13);
            this.labelUsername.TabIndex = 36;
            this.labelUsername.Text = "Username";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(115, 2);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 13);
            this.labelPassword.TabIndex = 37;
            this.labelPassword.Text = "Password";
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(224, 15);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(75, 23);
            this.buttonLogin.TabIndex = 2;
            this.buttonLogin.Text = "Log In";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // comboBoxNewTicketOwner
            // 
            this.comboBoxNewTicketOwner.FormattingEnabled = true;
            this.comboBoxNewTicketOwner.Location = new System.Drawing.Point(133, 248);
            this.comboBoxNewTicketOwner.Name = "comboBoxNewTicketOwner";
            this.comboBoxNewTicketOwner.Size = new System.Drawing.Size(120, 21);
            this.comboBoxNewTicketOwner.TabIndex = 39;
            // 
            // labelNewTicketOwner
            // 
            this.labelNewTicketOwner.AutoSize = true;
            this.labelNewTicketOwner.Location = new System.Drawing.Point(130, 233);
            this.labelNewTicketOwner.Name = "labelNewTicketOwner";
            this.labelNewTicketOwner.Size = new System.Drawing.Size(96, 13);
            this.labelNewTicketOwner.TabIndex = 40;
            this.labelNewTicketOwner.Text = "New Ticket Owner";
            // 
            // textSelectedTicketProjectTitle
            // 
            this.textSelectedTicketProjectTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSelectedTicketProjectTitle.Location = new System.Drawing.Point(336, 248);
            this.textSelectedTicketProjectTitle.Name = "textSelectedTicketProjectTitle";
            this.textSelectedTicketProjectTitle.ReadOnly = true;
            this.textSelectedTicketProjectTitle.Size = new System.Drawing.Size(115, 20);
            this.textSelectedTicketProjectTitle.TabIndex = 41;
            // 
            // labelTicketProjectTitle
            // 
            this.labelTicketProjectTitle.AutoSize = true;
            this.labelTicketProjectTitle.Location = new System.Drawing.Point(333, 233);
            this.labelTicketProjectTitle.Name = "labelTicketProjectTitle";
            this.labelTicketProjectTitle.Size = new System.Drawing.Size(96, 13);
            this.labelTicketProjectTitle.TabIndex = 42;
            this.labelTicketProjectTitle.Text = "Ticket Project Title";
            // 
            // textSelectedTicketTitle
            // 
            this.textSelectedTicketTitle.Location = new System.Drawing.Point(336, 285);
            this.textSelectedTicketTitle.Name = "textSelectedTicketTitle";
            this.textSelectedTicketTitle.Size = new System.Drawing.Size(115, 20);
            this.textSelectedTicketTitle.TabIndex = 43;
            // 
            // textSelectedTicketDescription
            // 
            this.textSelectedTicketDescription.Location = new System.Drawing.Point(336, 323);
            this.textSelectedTicketDescription.Multiline = true;
            this.textSelectedTicketDescription.Name = "textSelectedTicketDescription";
            this.textSelectedTicketDescription.Size = new System.Drawing.Size(115, 60);
            this.textSelectedTicketDescription.TabIndex = 44;
            // 
            // labelTicketTitle
            // 
            this.labelTicketTitle.AutoSize = true;
            this.labelTicketTitle.Location = new System.Drawing.Point(333, 270);
            this.labelTicketTitle.Name = "labelTicketTitle";
            this.labelTicketTitle.Size = new System.Drawing.Size(60, 13);
            this.labelTicketTitle.TabIndex = 45;
            this.labelTicketTitle.Text = "Ticket Title";
            // 
            // labelTicketDescription
            // 
            this.labelTicketDescription.AutoSize = true;
            this.labelTicketDescription.Location = new System.Drawing.Point(333, 308);
            this.labelTicketDescription.Name = "labelTicketDescription";
            this.labelTicketDescription.Size = new System.Drawing.Size(93, 13);
            this.labelTicketDescription.TabIndex = 46;
            this.labelTicketDescription.Text = "Ticket Description";
            // 
            // labelTicketOwner
            // 
            this.labelTicketOwner.AutoSize = true;
            this.labelTicketOwner.Location = new System.Drawing.Point(454, 270);
            this.labelTicketOwner.Name = "labelTicketOwner";
            this.labelTicketOwner.Size = new System.Drawing.Size(71, 13);
            this.labelTicketOwner.TabIndex = 48;
            this.labelTicketOwner.Text = "Ticket Owner";
            // 
            // labelTicketType
            // 
            this.labelTicketType.AutoSize = true;
            this.labelTicketType.Location = new System.Drawing.Point(454, 308);
            this.labelTicketType.Name = "labelTicketType";
            this.labelTicketType.Size = new System.Drawing.Size(64, 13);
            this.labelTicketType.TabIndex = 50;
            this.labelTicketType.Text = "Ticket Type";
            // 
            // labelTicketPriority
            // 
            this.labelTicketPriority.AutoSize = true;
            this.labelTicketPriority.Location = new System.Drawing.Point(454, 346);
            this.labelTicketPriority.Name = "labelTicketPriority";
            this.labelTicketPriority.Size = new System.Drawing.Size(71, 13);
            this.labelTicketPriority.TabIndex = 52;
            this.labelTicketPriority.Text = "Ticket Priority";
            // 
            // labelTicketStatus
            // 
            this.labelTicketStatus.AutoSize = true;
            this.labelTicketStatus.Location = new System.Drawing.Point(454, 385);
            this.labelTicketStatus.Name = "labelTicketStatus";
            this.labelTicketStatus.Size = new System.Drawing.Size(70, 13);
            this.labelTicketStatus.TabIndex = 54;
            this.labelTicketStatus.Text = "Ticket Status";
            // 
            // buttonShowTicketDetails
            // 
            this.buttonShowTicketDetails.Location = new System.Drawing.Point(459, 194);
            this.buttonShowTicketDetails.Name = "buttonShowTicketDetails";
            this.buttonShowTicketDetails.Size = new System.Drawing.Size(120, 23);
            this.buttonShowTicketDetails.TabIndex = 22;
            this.buttonShowTicketDetails.Text = "Ticket Details";
            this.buttonShowTicketDetails.UseVisualStyleBackColor = true;
            this.buttonShowTicketDetails.Click += new System.EventHandler(this.buttonShowTicketDetails_Click);
            // 
            // buttonAddComment
            // 
            this.buttonAddComment.Location = new System.Drawing.Point(336, 477);
            this.buttonAddComment.Name = "buttonAddComment";
            this.buttonAddComment.Size = new System.Drawing.Size(115, 23);
            this.buttonAddComment.TabIndex = 56;
            this.buttonAddComment.Text = "Add Comment";
            this.buttonAddComment.UseVisualStyleBackColor = true;
            this.buttonAddComment.Click += new System.EventHandler(this.buttonAddComment_Click);
            // 
            // textAddComment
            // 
            this.textAddComment.Location = new System.Drawing.Point(336, 432);
            this.textAddComment.Multiline = true;
            this.textAddComment.Name = "textAddComment";
            this.textAddComment.Size = new System.Drawing.Size(175, 40);
            this.textAddComment.TabIndex = 57;
            // 
            // labelAddComment
            // 
            this.labelAddComment.AutoSize = true;
            this.labelAddComment.Location = new System.Drawing.Point(333, 416);
            this.labelAddComment.Name = "labelAddComment";
            this.labelAddComment.Size = new System.Drawing.Size(73, 13);
            this.labelAddComment.TabIndex = 58;
            this.labelAddComment.Text = "Add Comment";
            // 
            // listBoxCommentList
            // 
            this.listBoxCommentList.FormattingEnabled = true;
            this.listBoxCommentList.Location = new System.Drawing.Point(586, 248);
            this.listBoxCommentList.Name = "listBoxCommentList";
            this.listBoxCommentList.Size = new System.Drawing.Size(120, 95);
            this.listBoxCommentList.TabIndex = 59;
            // 
            // labelCommentList
            // 
            this.labelCommentList.AutoSize = true;
            this.labelCommentList.Location = new System.Drawing.Point(583, 232);
            this.labelCommentList.Name = "labelCommentList";
            this.labelCommentList.Size = new System.Drawing.Size(89, 13);
            this.labelCommentList.TabIndex = 60;
            this.labelCommentList.Text = "Ticket Comments";
            // 
            // textViewComment
            // 
            this.textViewComment.Location = new System.Drawing.Point(526, 432);
            this.textViewComment.Multiline = true;
            this.textViewComment.Name = "textViewComment";
            this.textViewComment.Size = new System.Drawing.Size(175, 40);
            this.textViewComment.TabIndex = 61;
            // 
            // labelViewComment
            // 
            this.labelViewComment.AutoSize = true;
            this.labelViewComment.Location = new System.Drawing.Point(624, 416);
            this.labelViewComment.Name = "labelViewComment";
            this.labelViewComment.Size = new System.Drawing.Size(77, 13);
            this.labelViewComment.TabIndex = 62;
            this.labelViewComment.Text = "View Comment";
            // 
            // buttonViewComment
            // 
            this.buttonViewComment.Location = new System.Drawing.Point(586, 349);
            this.buttonViewComment.Name = "buttonViewComment";
            this.buttonViewComment.Size = new System.Drawing.Size(120, 23);
            this.buttonViewComment.TabIndex = 63;
            this.buttonViewComment.Text = "View Comment";
            this.buttonViewComment.UseVisualStyleBackColor = true;
            this.buttonViewComment.Click += new System.EventHandler(this.buttonViewComment_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(224, 44);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(75, 23);
            this.buttonLogout.TabIndex = 3;
            this.buttonLogout.Text = "Log Out";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // labelLoggedInUsername
            // 
            this.labelLoggedInUsername.AutoSize = true;
            this.labelLoggedInUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoggedInUsername.Location = new System.Drawing.Point(333, 15);
            this.labelLoggedInUsername.Name = "labelLoggedInUsername";
            this.labelLoggedInUsername.Size = new System.Drawing.Size(0, 13);
            this.labelLoggedInUsername.TabIndex = 64;
            // 
            // labelLoggedInUserRole
            // 
            this.labelLoggedInUserRole.AutoSize = true;
            this.labelLoggedInUserRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoggedInUserRole.Location = new System.Drawing.Point(451, 15);
            this.labelLoggedInUserRole.Name = "labelLoggedInUserRole";
            this.labelLoggedInUserRole.Size = new System.Drawing.Size(0, 13);
            this.labelLoggedInUserRole.TabIndex = 65;
            // 
            // buttonAddUser
            // 
            this.buttonAddUser.Location = new System.Drawing.Point(586, 194);
            this.buttonAddUser.Name = "buttonAddUser";
            this.buttonAddUser.Size = new System.Drawing.Size(86, 23);
            this.buttonAddUser.TabIndex = 66;
            this.buttonAddUser.Text = "Add New User";
            this.buttonAddUser.UseVisualStyleBackColor = true;
            this.buttonAddUser.Click += new System.EventHandler(this.buttonAddUser_Click);
            // 
            // textNewUsername
            // 
            this.textNewUsername.Location = new System.Drawing.Point(586, 86);
            this.textNewUsername.Name = "textNewUsername";
            this.textNewUsername.Size = new System.Drawing.Size(100, 20);
            this.textNewUsername.TabIndex = 67;
            // 
            // textNewUserPassword
            // 
            this.textNewUserPassword.Location = new System.Drawing.Point(586, 123);
            this.textNewUserPassword.Name = "textNewUserPassword";
            this.textNewUserPassword.Size = new System.Drawing.Size(100, 20);
            this.textNewUserPassword.TabIndex = 68;
            // 
            // comboBoxNewUserRole
            // 
            this.comboBoxNewUserRole.FormattingEnabled = true;
            this.comboBoxNewUserRole.Location = new System.Drawing.Point(586, 159);
            this.comboBoxNewUserRole.Name = "comboBoxNewUserRole";
            this.comboBoxNewUserRole.Size = new System.Drawing.Size(100, 21);
            this.comboBoxNewUserRole.TabIndex = 69;
            // 
            // labelNewUsername
            // 
            this.labelNewUsername.AutoSize = true;
            this.labelNewUsername.Location = new System.Drawing.Point(583, 70);
            this.labelNewUsername.Name = "labelNewUsername";
            this.labelNewUsername.Size = new System.Drawing.Size(80, 13);
            this.labelNewUsername.TabIndex = 70;
            this.labelNewUsername.Text = "New Username";
            // 
            // labelNewUserPassword
            // 
            this.labelNewUserPassword.AutoSize = true;
            this.labelNewUserPassword.Location = new System.Drawing.Point(583, 107);
            this.labelNewUserPassword.Name = "labelNewUserPassword";
            this.labelNewUserPassword.Size = new System.Drawing.Size(103, 13);
            this.labelNewUserPassword.TabIndex = 71;
            this.labelNewUserPassword.Text = "New User Password";
            // 
            // labelNewUserRole
            // 
            this.labelNewUserRole.AutoSize = true;
            this.labelNewUserRole.Location = new System.Drawing.Point(583, 143);
            this.labelNewUserRole.Name = "labelNewUserRole";
            this.labelNewUserRole.Size = new System.Drawing.Size(79, 13);
            this.labelNewUserRole.TabIndex = 72;
            this.labelNewUserRole.Text = "New User Role";
            // 
            // dateTimeSelectedProjectDeadline
            // 
            this.dateTimeSelectedProjectDeadline.Location = new System.Drawing.Point(335, 161);
            this.dateTimeSelectedProjectDeadline.Name = "dateTimeSelectedProjectDeadline";
            this.dateTimeSelectedProjectDeadline.Size = new System.Drawing.Size(115, 20);
            this.dateTimeSelectedProjectDeadline.TabIndex = 73;
            // 
            // buttonUpdateProject
            // 
            this.buttonUpdateProject.Location = new System.Drawing.Point(335, 194);
            this.buttonUpdateProject.Name = "buttonUpdateProject";
            this.buttonUpdateProject.Size = new System.Drawing.Size(113, 23);
            this.buttonUpdateProject.TabIndex = 74;
            this.buttonUpdateProject.Text = "Update Project";
            this.buttonUpdateProject.UseVisualStyleBackColor = true;
            this.buttonUpdateProject.Click += new System.EventHandler(this.buttonUpdateProject_Click);
            // 
            // comboBoxSelectedTicketType
            // 
            this.comboBoxSelectedTicketType.FormattingEnabled = true;
            this.comboBoxSelectedTicketType.Location = new System.Drawing.Point(457, 322);
            this.comboBoxSelectedTicketType.Name = "comboBoxSelectedTicketType";
            this.comboBoxSelectedTicketType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectedTicketType.TabIndex = 75;
            // 
            // comboBoxSelectedTicketPriority
            // 
            this.comboBoxSelectedTicketPriority.FormattingEnabled = true;
            this.comboBoxSelectedTicketPriority.Location = new System.Drawing.Point(457, 361);
            this.comboBoxSelectedTicketPriority.Name = "comboBoxSelectedTicketPriority";
            this.comboBoxSelectedTicketPriority.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectedTicketPriority.TabIndex = 76;
            // 
            // comboBoxSelectedTicketStatus
            // 
            this.comboBoxSelectedTicketStatus.FormattingEnabled = true;
            this.comboBoxSelectedTicketStatus.Location = new System.Drawing.Point(457, 400);
            this.comboBoxSelectedTicketStatus.Name = "comboBoxSelectedTicketStatus";
            this.comboBoxSelectedTicketStatus.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectedTicketStatus.TabIndex = 77;
            // 
            // comboBoxSelectedTicketOwner
            // 
            this.comboBoxSelectedTicketOwner.FormattingEnabled = true;
            this.comboBoxSelectedTicketOwner.Location = new System.Drawing.Point(457, 285);
            this.comboBoxSelectedTicketOwner.Name = "comboBoxSelectedTicketOwner";
            this.comboBoxSelectedTicketOwner.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectedTicketOwner.TabIndex = 78;
            // 
            // buttonEditTicket
            // 
            this.buttonEditTicket.Location = new System.Drawing.Point(335, 390);
            this.buttonEditTicket.Name = "buttonEditTicket";
            this.buttonEditTicket.Size = new System.Drawing.Size(113, 23);
            this.buttonEditTicket.TabIndex = 79;
            this.buttonEditTicket.Text = "Edit Ticket";
            this.buttonEditTicket.UseVisualStyleBackColor = true;
            this.buttonEditTicket.Click += new System.EventHandler(this.buttonEditTicket_Click);
            // 
            // listBoxFilterTickets
            // 
            this.listBoxFilterTickets.FormattingEnabled = true;
            this.listBoxFilterTickets.Location = new System.Drawing.Point(718, 85);
            this.listBoxFilterTickets.Name = "listBoxFilterTickets";
            this.listBoxFilterTickets.Size = new System.Drawing.Size(120, 95);
            this.listBoxFilterTickets.TabIndex = 80;
            // 
            // comboBoxFilterOpenTicketType
            // 
            this.comboBoxFilterOpenTicketType.FormattingEnabled = true;
            this.comboBoxFilterOpenTicketType.Location = new System.Drawing.Point(717, 235);
            this.comboBoxFilterOpenTicketType.Name = "comboBoxFilterOpenTicketType";
            this.comboBoxFilterOpenTicketType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterOpenTicketType.TabIndex = 81;
            // 
            // labelFilterOpenTicketType
            // 
            this.labelFilterOpenTicketType.AutoSize = true;
            this.labelFilterOpenTicketType.Location = new System.Drawing.Point(714, 219);
            this.labelFilterOpenTicketType.Name = "labelFilterOpenTicketType";
            this.labelFilterOpenTicketType.Size = new System.Drawing.Size(225, 13);
            this.labelFilterOpenTicketType.TabIndex = 82;
            this.labelFilterOpenTicketType.Text = "Filter Selected Project\'s Open Tickets by Type";
            // 
            // buttonFilterOpenTicketType
            // 
            this.buttonFilterOpenTicketType.Location = new System.Drawing.Point(858, 233);
            this.buttonFilterOpenTicketType.Name = "buttonFilterOpenTicketType";
            this.buttonFilterOpenTicketType.Size = new System.Drawing.Size(75, 23);
            this.buttonFilterOpenTicketType.TabIndex = 83;
            this.buttonFilterOpenTicketType.Text = "Go";
            this.buttonFilterOpenTicketType.UseVisualStyleBackColor = true;
            this.buttonFilterOpenTicketType.Click += new System.EventHandler(this.buttonFilterOpenTicketType_Click);
            // 
            // comboBoxFilterOpenTicketPriority
            // 
            this.comboBoxFilterOpenTicketPriority.FormattingEnabled = true;
            this.comboBoxFilterOpenTicketPriority.Location = new System.Drawing.Point(717, 277);
            this.comboBoxFilterOpenTicketPriority.Name = "comboBoxFilterOpenTicketPriority";
            this.comboBoxFilterOpenTicketPriority.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterOpenTicketPriority.TabIndex = 84;
            // 
            // labelFilterOpenTicketPriority
            // 
            this.labelFilterOpenTicketPriority.AutoSize = true;
            this.labelFilterOpenTicketPriority.Location = new System.Drawing.Point(714, 262);
            this.labelFilterOpenTicketPriority.Name = "labelFilterOpenTicketPriority";
            this.labelFilterOpenTicketPriority.Size = new System.Drawing.Size(232, 13);
            this.labelFilterOpenTicketPriority.TabIndex = 85;
            this.labelFilterOpenTicketPriority.Text = "Filter Selected Project\'s Open Tickets by Priority";
            // 
            // buttonFilterOpenTicketPriority
            // 
            this.buttonFilterOpenTicketPriority.Location = new System.Drawing.Point(858, 275);
            this.buttonFilterOpenTicketPriority.Name = "buttonFilterOpenTicketPriority";
            this.buttonFilterOpenTicketPriority.Size = new System.Drawing.Size(75, 23);
            this.buttonFilterOpenTicketPriority.TabIndex = 86;
            this.buttonFilterOpenTicketPriority.Text = "Go";
            this.buttonFilterOpenTicketPriority.UseVisualStyleBackColor = true;
            this.buttonFilterOpenTicketPriority.Click += new System.EventHandler(this.buttonFilterOpenTicketPriority_Click);
            // 
            // labelFilterTickets
            // 
            this.labelFilterTickets.AutoSize = true;
            this.labelFilterTickets.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilterTickets.Location = new System.Drawing.Point(715, 70);
            this.labelFilterTickets.Name = "labelFilterTickets";
            this.labelFilterTickets.Size = new System.Drawing.Size(0, 13);
            this.labelFilterTickets.TabIndex = 87;
            // 
            // comboBoxFilterWaitingTicketType
            // 
            this.comboBoxFilterWaitingTicketType.FormattingEnabled = true;
            this.comboBoxFilterWaitingTicketType.Location = new System.Drawing.Point(718, 315);
            this.comboBoxFilterWaitingTicketType.Name = "comboBoxFilterWaitingTicketType";
            this.comboBoxFilterWaitingTicketType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterWaitingTicketType.TabIndex = 88;
            // 
            // labelFilterWaitingTicketType
            // 
            this.labelFilterWaitingTicketType.AutoSize = true;
            this.labelFilterWaitingTicketType.Location = new System.Drawing.Point(714, 300);
            this.labelFilterWaitingTicketType.Name = "labelFilterWaitingTicketType";
            this.labelFilterWaitingTicketType.Size = new System.Drawing.Size(235, 13);
            this.labelFilterWaitingTicketType.TabIndex = 89;
            this.labelFilterWaitingTicketType.Text = "Filter Selected Project\'s Waiting Tickets by Type";
            // 
            // labelFilterWaitingTicketPriority
            // 
            this.labelFilterWaitingTicketPriority.AutoSize = true;
            this.labelFilterWaitingTicketPriority.Location = new System.Drawing.Point(715, 339);
            this.labelFilterWaitingTicketPriority.Name = "labelFilterWaitingTicketPriority";
            this.labelFilterWaitingTicketPriority.Size = new System.Drawing.Size(242, 13);
            this.labelFilterWaitingTicketPriority.TabIndex = 90;
            this.labelFilterWaitingTicketPriority.Text = "Filter Selected Project\'s Waiting Tickets by Priority";
            // 
            // comboBoxFilterWaitingTicketPriority
            // 
            this.comboBoxFilterWaitingTicketPriority.FormattingEnabled = true;
            this.comboBoxFilterWaitingTicketPriority.Location = new System.Drawing.Point(718, 355);
            this.comboBoxFilterWaitingTicketPriority.Name = "comboBoxFilterWaitingTicketPriority";
            this.comboBoxFilterWaitingTicketPriority.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterWaitingTicketPriority.TabIndex = 91;
            // 
            // buttonFilterWaitingTicketType
            // 
            this.buttonFilterWaitingTicketType.Location = new System.Drawing.Point(858, 314);
            this.buttonFilterWaitingTicketType.Name = "buttonFilterWaitingTicketType";
            this.buttonFilterWaitingTicketType.Size = new System.Drawing.Size(75, 23);
            this.buttonFilterWaitingTicketType.TabIndex = 92;
            this.buttonFilterWaitingTicketType.Text = "Go";
            this.buttonFilterWaitingTicketType.UseVisualStyleBackColor = true;
            this.buttonFilterWaitingTicketType.Click += new System.EventHandler(this.buttonFilterWaitingTicketType_Click);
            // 
            // buttonFilterWaitingTicketPriority
            // 
            this.buttonFilterWaitingTicketPriority.Location = new System.Drawing.Point(858, 354);
            this.buttonFilterWaitingTicketPriority.Name = "buttonFilterWaitingTicketPriority";
            this.buttonFilterWaitingTicketPriority.Size = new System.Drawing.Size(75, 23);
            this.buttonFilterWaitingTicketPriority.TabIndex = 93;
            this.buttonFilterWaitingTicketPriority.Text = "Go";
            this.buttonFilterWaitingTicketPriority.UseVisualStyleBackColor = true;
            this.buttonFilterWaitingTicketPriority.Click += new System.EventHandler(this.buttonFilterWaitingTicketPriority_Click);
            // 
            // comboBoxFilterUserOpenTickets
            // 
            this.comboBoxFilterUserOpenTickets.FormattingEnabled = true;
            this.comboBoxFilterUserOpenTickets.Location = new System.Drawing.Point(718, 395);
            this.comboBoxFilterUserOpenTickets.Name = "comboBoxFilterUserOpenTickets";
            this.comboBoxFilterUserOpenTickets.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterUserOpenTickets.TabIndex = 94;
            // 
            // labelFilterUserOpenTickets
            // 
            this.labelFilterUserOpenTickets.AutoSize = true;
            this.labelFilterUserOpenTickets.Location = new System.Drawing.Point(715, 379);
            this.labelFilterUserOpenTickets.Name = "labelFilterUserOpenTickets";
            this.labelFilterUserOpenTickets.Size = new System.Drawing.Size(149, 13);
            this.labelFilterUserOpenTickets.TabIndex = 95;
            this.labelFilterUserOpenTickets.Text = "Filter All Open Tickets by User";
            // 
            // buttonFilterUserOpenTickets
            // 
            this.buttonFilterUserOpenTickets.Location = new System.Drawing.Point(858, 394);
            this.buttonFilterUserOpenTickets.Name = "buttonFilterUserOpenTickets";
            this.buttonFilterUserOpenTickets.Size = new System.Drawing.Size(75, 23);
            this.buttonFilterUserOpenTickets.TabIndex = 96;
            this.buttonFilterUserOpenTickets.Text = "Go";
            this.buttonFilterUserOpenTickets.UseVisualStyleBackColor = true;
            this.buttonFilterUserOpenTickets.Click += new System.EventHandler(this.buttonFilterUserOpenTickets_Click);
            // 
            // comboBoxFilterUserWaitingTickets
            // 
            this.comboBoxFilterUserWaitingTickets.FormattingEnabled = true;
            this.comboBoxFilterUserWaitingTickets.Location = new System.Drawing.Point(717, 435);
            this.comboBoxFilterUserWaitingTickets.Name = "comboBoxFilterUserWaitingTickets";
            this.comboBoxFilterUserWaitingTickets.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterUserWaitingTickets.TabIndex = 97;
            // 
            // labelFilterUserWaitingTickets
            // 
            this.labelFilterUserWaitingTickets.AutoSize = true;
            this.labelFilterUserWaitingTickets.Location = new System.Drawing.Point(715, 419);
            this.labelFilterUserWaitingTickets.Name = "labelFilterUserWaitingTickets";
            this.labelFilterUserWaitingTickets.Size = new System.Drawing.Size(159, 13);
            this.labelFilterUserWaitingTickets.TabIndex = 98;
            this.labelFilterUserWaitingTickets.Text = "Filter All Waiting Tickets by User";
            // 
            // buttonFilterUserWaitingTickets
            // 
            this.buttonFilterUserWaitingTickets.Location = new System.Drawing.Point(858, 434);
            this.buttonFilterUserWaitingTickets.Name = "buttonFilterUserWaitingTickets";
            this.buttonFilterUserWaitingTickets.Size = new System.Drawing.Size(75, 23);
            this.buttonFilterUserWaitingTickets.TabIndex = 99;
            this.buttonFilterUserWaitingTickets.Text = "Go";
            this.buttonFilterUserWaitingTickets.UseVisualStyleBackColor = true;
            this.buttonFilterUserWaitingTickets.Click += new System.EventHandler(this.buttonFilterUserWaitingTickets_Click);
            // 
            // buttonShowFilterTicketDetails
            // 
            this.buttonShowFilterTicketDetails.Location = new System.Drawing.Point(717, 194);
            this.buttonShowFilterTicketDetails.Name = "buttonShowFilterTicketDetails";
            this.buttonShowFilterTicketDetails.Size = new System.Drawing.Size(120, 23);
            this.buttonShowFilterTicketDetails.TabIndex = 100;
            this.buttonShowFilterTicketDetails.Text = "Show Details";
            this.buttonShowFilterTicketDetails.UseVisualStyleBackColor = true;
            this.buttonShowFilterTicketDetails.Click += new System.EventHandler(this.buttonShowFilterTicketDetails_Click);
            // 
            // textSelectedTicketID
            // 
            this.textSelectedTicketID.Location = new System.Drawing.Point(457, 247);
            this.textSelectedTicketID.Name = "textSelectedTicketID";
            this.textSelectedTicketID.ReadOnly = true;
            this.textSelectedTicketID.Size = new System.Drawing.Size(48, 20);
            this.textSelectedTicketID.TabIndex = 101;
            // 
            // labelTicketID
            // 
            this.labelTicketID.AutoSize = true;
            this.labelTicketID.Location = new System.Drawing.Point(454, 232);
            this.labelTicketID.Name = "labelTicketID";
            this.labelTicketID.Size = new System.Drawing.Size(51, 13);
            this.labelTicketID.TabIndex = 102;
            this.labelTicketID.Text = "Ticket ID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 511);
            this.Controls.Add(this.labelTicketID);
            this.Controls.Add(this.textSelectedTicketID);
            this.Controls.Add(this.buttonShowFilterTicketDetails);
            this.Controls.Add(this.buttonFilterUserWaitingTickets);
            this.Controls.Add(this.labelFilterUserWaitingTickets);
            this.Controls.Add(this.comboBoxFilterUserWaitingTickets);
            this.Controls.Add(this.buttonFilterUserOpenTickets);
            this.Controls.Add(this.labelFilterUserOpenTickets);
            this.Controls.Add(this.comboBoxFilterUserOpenTickets);
            this.Controls.Add(this.buttonFilterWaitingTicketPriority);
            this.Controls.Add(this.buttonFilterWaitingTicketType);
            this.Controls.Add(this.comboBoxFilterWaitingTicketPriority);
            this.Controls.Add(this.labelFilterWaitingTicketPriority);
            this.Controls.Add(this.labelFilterWaitingTicketType);
            this.Controls.Add(this.comboBoxFilterWaitingTicketType);
            this.Controls.Add(this.labelFilterTickets);
            this.Controls.Add(this.buttonFilterOpenTicketPriority);
            this.Controls.Add(this.labelFilterOpenTicketPriority);
            this.Controls.Add(this.comboBoxFilterOpenTicketPriority);
            this.Controls.Add(this.buttonFilterOpenTicketType);
            this.Controls.Add(this.labelFilterOpenTicketType);
            this.Controls.Add(this.comboBoxFilterOpenTicketType);
            this.Controls.Add(this.listBoxFilterTickets);
            this.Controls.Add(this.buttonEditTicket);
            this.Controls.Add(this.comboBoxSelectedTicketOwner);
            this.Controls.Add(this.comboBoxSelectedTicketStatus);
            this.Controls.Add(this.comboBoxSelectedTicketPriority);
            this.Controls.Add(this.comboBoxSelectedTicketType);
            this.Controls.Add(this.buttonUpdateProject);
            this.Controls.Add(this.dateTimeSelectedProjectDeadline);
            this.Controls.Add(this.labelNewUserRole);
            this.Controls.Add(this.labelNewUserPassword);
            this.Controls.Add(this.labelNewUsername);
            this.Controls.Add(this.comboBoxNewUserRole);
            this.Controls.Add(this.textNewUserPassword);
            this.Controls.Add(this.textNewUsername);
            this.Controls.Add(this.buttonAddUser);
            this.Controls.Add(this.labelLoggedInUserRole);
            this.Controls.Add(this.labelLoggedInUsername);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonViewComment);
            this.Controls.Add(this.labelViewComment);
            this.Controls.Add(this.textViewComment);
            this.Controls.Add(this.labelCommentList);
            this.Controls.Add(this.listBoxCommentList);
            this.Controls.Add(this.labelAddComment);
            this.Controls.Add(this.textAddComment);
            this.Controls.Add(this.buttonAddComment);
            this.Controls.Add(this.buttonShowTicketDetails);
            this.Controls.Add(this.labelTicketStatus);
            this.Controls.Add(this.labelTicketPriority);
            this.Controls.Add(this.labelTicketType);
            this.Controls.Add(this.labelTicketOwner);
            this.Controls.Add(this.labelTicketDescription);
            this.Controls.Add(this.labelTicketTitle);
            this.Controls.Add(this.textSelectedTicketDescription);
            this.Controls.Add(this.textSelectedTicketTitle);
            this.Controls.Add(this.labelTicketProjectTitle);
            this.Controls.Add(this.textSelectedTicketProjectTitle);
            this.Controls.Add(this.labelNewTicketOwner);
            this.Controls.Add(this.comboBoxNewTicketOwner);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.textUsername);
            this.Controls.Add(this.labelProjectTickets);
            this.Controls.Add(this.comboBoxNewTicketStatus);
            this.Controls.Add(this.comboBoxNewTicketType);
            this.Controls.Add(this.comboBoxNewTicketPriority);
            this.Controls.Add(this.buttonCreateTicket);
            this.Controls.Add(this.labelNewTicketType);
            this.Controls.Add(this.labelNewTicketStatus);
            this.Controls.Add(this.labelNewTicketPriority);
            this.Controls.Add(this.labelNewTicketDescription);
            this.Controls.Add(this.labelNewTicketTitle);
            this.Controls.Add(this.textNewTicketDescription);
            this.Controls.Add(this.textNewTicketTitle);
            this.Controls.Add(this.listBoxTicketList);
            this.Controls.Add(this.labelProjectDeadline);
            this.Controls.Add(this.labelProjectDescription);
            this.Controls.Add(this.labelProjectTitle);
            this.Controls.Add(this.buttonShowProjectDetails);
            this.Controls.Add(this.textSelectedProjectDescription);
            this.Controls.Add(this.textSelectedProjectTitle);
            this.Controls.Add(this.labelProjectList);
            this.Controls.Add(this.labelNewProjectDeadline);
            this.Controls.Add(this.labelNewProjectDescription);
            this.Controls.Add(this.labelNewProjectTitle);
            this.Controls.Add(this.listBoxProjectList);
            this.Controls.Add(this.dateTimeProjectDeadline);
            this.Controls.Add(this.buttonCreateProject);
            this.Controls.Add(this.textProjectDescription);
            this.Controls.Add(this.textProjectTitle);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textProjectTitle;
        private System.Windows.Forms.TextBox textProjectDescription;
        private System.Windows.Forms.Button buttonCreateProject;
        private System.Windows.Forms.DateTimePicker dateTimeProjectDeadline;
        private System.Windows.Forms.ListBox listBoxProjectList;
        private System.Windows.Forms.Label labelNewProjectTitle;
        private System.Windows.Forms.Label labelNewProjectDescription;
        private System.Windows.Forms.Label labelNewProjectDeadline;
        private System.Windows.Forms.Label labelProjectList;
        private System.Windows.Forms.TextBox textSelectedProjectTitle;
        private System.Windows.Forms.TextBox textSelectedProjectDescription;
        private System.Windows.Forms.Button buttonShowProjectDetails;
        private System.Windows.Forms.Label labelProjectTitle;
        private System.Windows.Forms.Label labelProjectDescription;
        private System.Windows.Forms.Label labelProjectDeadline;
        private System.Windows.Forms.ListBox listBoxTicketList;
        private System.Windows.Forms.TextBox textNewTicketTitle;
        private System.Windows.Forms.TextBox textNewTicketDescription;
        private System.Windows.Forms.Label labelNewTicketTitle;
        private System.Windows.Forms.Label labelNewTicketDescription;
        private System.Windows.Forms.Label labelNewTicketPriority;
        private System.Windows.Forms.Label labelNewTicketStatus;
        private System.Windows.Forms.Label labelNewTicketType;
        private System.Windows.Forms.Button buttonCreateTicket;
        private System.Windows.Forms.ComboBox comboBoxNewTicketPriority;
        private System.Windows.Forms.ComboBox comboBoxNewTicketType;
        private System.Windows.Forms.ComboBox comboBoxNewTicketStatus;
        private System.Windows.Forms.Label labelProjectTickets;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.ComboBox comboBoxNewTicketOwner;
        private System.Windows.Forms.Label labelNewTicketOwner;
        private System.Windows.Forms.TextBox textSelectedTicketProjectTitle;
        private System.Windows.Forms.Label labelTicketProjectTitle;
        private System.Windows.Forms.TextBox textSelectedTicketTitle;
        private System.Windows.Forms.TextBox textSelectedTicketDescription;
        private System.Windows.Forms.Label labelTicketTitle;
        private System.Windows.Forms.Label labelTicketDescription;
        private System.Windows.Forms.Label labelTicketOwner;
        private System.Windows.Forms.Label labelTicketType;
        private System.Windows.Forms.Label labelTicketPriority;
        private System.Windows.Forms.Label labelTicketStatus;
        private System.Windows.Forms.Button buttonShowTicketDetails;
        private System.Windows.Forms.Button buttonAddComment;
        private System.Windows.Forms.TextBox textAddComment;
        private System.Windows.Forms.Label labelAddComment;
        private System.Windows.Forms.ListBox listBoxCommentList;
        private System.Windows.Forms.Label labelCommentList;
        private System.Windows.Forms.TextBox textViewComment;
        private System.Windows.Forms.Label labelViewComment;
        private System.Windows.Forms.Button buttonViewComment;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Label labelLoggedInUsername;
        private System.Windows.Forms.Label labelLoggedInUserRole;
        private System.Windows.Forms.Button buttonAddUser;
        private System.Windows.Forms.TextBox textNewUsername;
        private System.Windows.Forms.TextBox textNewUserPassword;
        private System.Windows.Forms.ComboBox comboBoxNewUserRole;
        private System.Windows.Forms.Label labelNewUsername;
        private System.Windows.Forms.Label labelNewUserPassword;
        private System.Windows.Forms.Label labelNewUserRole;
        private System.Windows.Forms.DateTimePicker dateTimeSelectedProjectDeadline;
        private System.Windows.Forms.Button buttonUpdateProject;
        private System.Windows.Forms.ComboBox comboBoxSelectedTicketType;
        private System.Windows.Forms.ComboBox comboBoxSelectedTicketPriority;
        private System.Windows.Forms.ComboBox comboBoxSelectedTicketStatus;
        private System.Windows.Forms.ComboBox comboBoxSelectedTicketOwner;
        private System.Windows.Forms.Button buttonEditTicket;
        private System.Windows.Forms.ListBox listBoxFilterTickets;
        private System.Windows.Forms.ComboBox comboBoxFilterOpenTicketType;
        private System.Windows.Forms.Label labelFilterOpenTicketType;
        private System.Windows.Forms.Button buttonFilterOpenTicketType;
        private System.Windows.Forms.ComboBox comboBoxFilterOpenTicketPriority;
        private System.Windows.Forms.Label labelFilterOpenTicketPriority;
        private System.Windows.Forms.Button buttonFilterOpenTicketPriority;
        private System.Windows.Forms.Label labelFilterTickets;
        private System.Windows.Forms.ComboBox comboBoxFilterWaitingTicketType;
        private System.Windows.Forms.Label labelFilterWaitingTicketType;
        private System.Windows.Forms.Label labelFilterWaitingTicketPriority;
        private System.Windows.Forms.ComboBox comboBoxFilterWaitingTicketPriority;
        private System.Windows.Forms.Button buttonFilterWaitingTicketType;
        private System.Windows.Forms.Button buttonFilterWaitingTicketPriority;
        private System.Windows.Forms.ComboBox comboBoxFilterUserOpenTickets;
        private System.Windows.Forms.Label labelFilterUserOpenTickets;
        private System.Windows.Forms.Button buttonFilterUserOpenTickets;
        private System.Windows.Forms.ComboBox comboBoxFilterUserWaitingTickets;
        private System.Windows.Forms.Label labelFilterUserWaitingTickets;
        private System.Windows.Forms.Button buttonFilterUserWaitingTickets;
        private System.Windows.Forms.Button buttonShowFilterTicketDetails;
        private System.Windows.Forms.TextBox textSelectedTicketID;
        private System.Windows.Forms.Label labelTicketID;
    }
}

