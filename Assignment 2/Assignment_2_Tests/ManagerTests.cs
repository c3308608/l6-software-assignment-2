﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 02/01/2015
// Description: Unit tests for methods in Manager class.
// Version    : 1.0
//-----------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_2;

namespace Assignment_2_Tests
{
    /// <summary>
    /// A unit test class for testing methods in the Manager class.
    /// </summary>
    [TestClass]
    public class ManagerTests
    {
        /// <summary>
        /// Checks that the method GetLoginStatus returns the correct boolean value when the 
        /// manager's log in status has been set to true.
        /// </summary>
        [TestMethod]
        public void GetLoginStatusReturnsCorrectStatusWhenManagerIsLoggedIn()
        {
            Manager manager = new Manager();

            manager.SetLoginStatus(true);

            bool loginStatus = manager.GetLoginStatus();

            Assert.AreEqual(true, loginStatus);
        }

        /// <summary>
        /// Checks that the method GetLoginStatus returns the correct boolean value when the 
        /// manager's log in status has been set to false.
        /// </summary>
        [TestMethod]
        public void GetLoginStatusReturnsCorrectStatusWhenManagerIsLoggedOut()
        {
            Manager manager = new Manager();

            manager.SetLoginStatus(false);

            bool loginStatus = manager.GetLoginStatus();

            Assert.AreEqual(false, loginStatus);
        }

    }
}
