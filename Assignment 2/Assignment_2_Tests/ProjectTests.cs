﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 09/12/2014
// Description: Unit tests for methods in Project class.
// Version    : 1.0
//-----------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_2;

namespace Assignment_2_Tests
{
    /// <summary>
    /// A unit test class for testing methods in the Project class.
    /// </summary>
    [TestClass]
    public class ProjectTests
    {
        /// <summary>
        /// Checks that the boolean method CheckNewProjectDetails returns true when a project is created with 
        /// valid details.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsTrueWhenProjectDetailsAreValid()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project("Test Title", "Test Description", testDeadline);

            bool goodInputs = testProject.CheckNewProjectDetails();

            Assert.IsTrue(goodInputs);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// an empty string as a title.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenProjectTitleIsEmptyString()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project("", "Test Description", testDeadline);

            bool badTitleInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badTitleInput);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// a string containing only white space as a title.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenProjectTitleIsWhiteSpace()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project("          ", "Test Description", testDeadline);

            bool badTitleInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badTitleInput);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// an empty string as a description.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenProjectDescriptionIsEmptyString()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project("Test Title", "", testDeadline);

            bool badDescriptionInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badDescriptionInput);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// a string containing only white space as a description.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenDescriptionIsWhiteSpace()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project("Test Title", "          ", testDeadline);

            bool badDescriptionInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badDescriptionInput);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// null as a title.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenProjectTitleIsNull()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project(null, "Test Description", testDeadline);

            bool badTitleInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badTitleInput);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// null as a description.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenDescriptionIsNull()
        {
            DateTime testDeadline = DateTime.Now;

            Project testProject = new Project("Test Title", null, testDeadline);

            bool badDescriptionInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badDescriptionInput);
        }

        /// <summary>
        /// Checks the boolean method CheckNewProjectDetails returns false when a project is created with 
        /// a deadline that is in the past.
        /// </summary>
        [TestMethod]
        public void CheckNewProjectDetailsReturnsFalseWhenProjectDeadlineIsInThePast()
        {
            DateTime pastDeadline = DateTime.Now.AddDays(-1);

            Project testProject = new Project("Test Title", "Test Description", pastDeadline);

            bool badDeadlineInput = testProject.CheckNewProjectDetails();

            Assert.IsFalse(badDeadlineInput);
        }
    }
}
