﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 05/01/2015
// Description: Unit tests for methods in Comment class.
// Version    : 1.0
//-----------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_2;

namespace Assignment_2_Tests
{
    /// <summary>
    /// A unit test class for testing methods in the Comment class.
    /// </summary>
    [TestClass]
    public class CommentTests
    {
        /// <summary>
        /// Checks that the boolean method CheckNewCommentDetails returns true when a comment is created with 
        /// valid details.
        /// </summary>
        [TestMethod]
        public void CheckNewCommentDetailsReturnsTrueWhenCommentDetailsAreValid()
        {
            Comment testComment = new Comment("This is a test comment", 100);

            bool goodInputs = testComment.CheckNewCommentDetails();

            Assert.IsTrue(goodInputs);
        }

        /// <summary>
        /// Checks the boolean method CheckNewCommentDetails returns false when a comment is created with 
        /// an empty string as it's text.
        /// </summary>
        [TestMethod]
        public void CheckNewCommentDetailsReturnsFalseWhenCommentTextIsEmptyString()
        {
            Comment testComment = new Comment("", 100);

            bool badText = testComment.CheckNewCommentDetails();

            Assert.IsFalse(badText);
        }

        /// <summary>
        /// Checks the boolean method CheckNewCommentDetails returns false when a comment is created with 
        /// only white space as it's text.
        /// </summary>
        [TestMethod]
        public void CheckNewCommentDetailsReturnsFalseWhenCommentTextIsWhiteSpace()
        {
            Comment testComment = new Comment("                   ", 100);

            bool badText = testComment.CheckNewCommentDetails();

            Assert.IsFalse(badText);
        }
    }
}
