﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 02/01/2015
// Description: Unit tests for methods in Ticket class.
// Version    : 1.0
//-----------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_2;

namespace Assignment_2_Tests
{
    /// <summary>
    /// A unit test class for testing methods in the Ticket class.
    /// </summary>
    [TestClass]
    public class TicketTests
    {
        /// <summary>
        /// Checks that the method TicketPriorityInt returns the correct enum integer value when the ticket priority
        /// is set as Critical.
        /// </summary>
        [TestMethod]
        public void TicketPriorityIntReturnsCorrectEnumValueForCriticalPriority()
        {
            Ticket testTicket = new Ticket();

            TicketPriority critical = (TicketPriority)testTicket.TicketPriorityInt("Critical");

            Assert.AreEqual(TicketPriority.Critical, critical);
        }

        /// <summary>
        /// Checks that the method TicketPriorityInt returns the correct enum integer value when the ticket priority
        /// is set as High.
        /// </summary>
        [TestMethod]
        public void TicketPriorityIntReturnsCorrectEnumValueForHighPriority()
        {
            Ticket testTicket = new Ticket();

            TicketPriority high = (TicketPriority)testTicket.TicketPriorityInt("High");

            Assert.AreEqual(TicketPriority.High, high);
        }

        /// <summary>
        /// Checks that the method TicketPriorityString returns the correct enum integer value when the ticket priority
        /// is set as Medium.
        /// </summary>
        [TestMethod]
        public void TicketPriorityIntReturnsCorrectEnumValueForMediumPriority()
        {
            Ticket testTicket = new Ticket();

            TicketPriority medium = (TicketPriority)testTicket.TicketPriorityInt("Medium");

            Assert.AreEqual(TicketPriority.Medium, medium);
        }

        /// <summary>
        /// Checks that the method TicketPriorityInt returns the correct enum integer value when the ticket priority
        /// is set as Low.
        /// </summary>
        [TestMethod]
        public void TicketPriorityIntReturnsCorrectEnumValueForLowPriority()
        {
            Ticket testTicket = new Ticket();

            TicketPriority low = (TicketPriority)testTicket.TicketPriorityInt("Low");

            Assert.AreEqual(TicketPriority.Low, low);
        }

        /// <summary>
        /// Checks that the method TicketStatusInt returns the correct enum integer value when the ticket status
        /// is set as Open.
        /// </summary>
        [TestMethod]
        public void TicketStatusIntReturnsCorrectEnumValueForOpenStatus()
        {
            Ticket testTicket = new Ticket();

            TicketStatus open = (TicketStatus)testTicket.TicketStatusInt("Open");

            Assert.AreEqual(TicketStatus.Open, open);
        }

        /// <summary>
        /// Checks that the method TicketStatusInt returns the correct enum integer value when the ticket status
        /// is set as Waiting.
        /// </summary>
        [TestMethod]
        public void TicketStatusIntReturnsCorrectEnumValueForWaitingStatus()
        {
            Ticket testTicket = new Ticket();

            TicketStatus waiting = (TicketStatus)testTicket.TicketStatusInt("Waiting");

            Assert.AreEqual(TicketStatus.Waiting, waiting);
        }

        /// <summary>
        /// Checks that the method TicketStatusInt returns the correct enum integer value when the ticket status
        /// is set as Closed.
        /// </summary>
        [TestMethod]
        public void TicketStatusIntReturnsCorrectEnumValueForClosedStatus()
        {
            Ticket testTicket = new Ticket();

            TicketStatus closed = (TicketStatus)testTicket.TicketStatusInt("Closed");

            Assert.AreEqual(TicketStatus.Closed, closed);
        }

        /// <summary>
        /// Checks that the method TicketTypeInt returns the correct enum integer value when the ticket type
        /// is set as Requirement.
        /// </summary>
        [TestMethod]
        public void TicketTypeIntReturnsCorrectEnumValueForRequirementType()
        {
            Ticket testTicket = new Ticket();

            TicketType requirement = (TicketType)testTicket.TicketTypeInt("Requirement");

            Assert.AreEqual(TicketType.Requirement, requirement);
        }

        /// <summary>
        /// Checks that the method TicketTypeInt returns the correct enum integer value when the ticket type
        /// is set as Issue.
        /// </summary>
        [TestMethod]
        public void TicketTypeIntReturnsCorrectEnumValueForIssueType()
        {
            Ticket testTicket = new Ticket();

            TicketType issue = (TicketType)testTicket.TicketTypeInt("Issue");

            Assert.AreEqual(TicketType.Issue, issue);
        }
    }
}
