﻿//-----------------------------------------------
// Author     : Tom Chambers
// Created    : 02/01/2015
// Description: Unit tests for methods in Developer class.
// Version    : 1.0
//-----------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_2;

namespace Assignment_2_Tests
{
    /// <summary>
    /// A unit test class for testing methods in the Developer class.
    /// </summary>
    [TestClass]
    public class DeveloperTests
    {
        /// <summary>
        /// Checks that the method GetLoginStatus returns the correct boolean value when the 
        /// developer's log in status has been set to true.
        /// </summary>
        [TestMethod]
        public void GetLoginStatusReturnsCorrectStatusWhenDeveloperIsLoggedIn()
        {
            Developer developer = new Developer();

            developer.SetLoginStatus(true);

            bool loginStatus = developer.GetLoginStatus();

            Assert.AreEqual(true, loginStatus);
        }

        /// <summary>
        /// Checks that the method GetLoginStatus returns the correct boolean value when the 
        /// developer's log in status has been set to false.
        /// </summary>
        [TestMethod]
        public void GetLoginStatusReturnsCorrectStatusWhenDeveloperIsLoggedOut()
        {
            Developer developer = new Developer();

            developer.SetLoginStatus(false);

            bool loginStatus = developer.GetLoginStatus();

            Assert.AreEqual(false, loginStatus);
        }
    }
}
